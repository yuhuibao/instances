module gitlab.com/yuhuibao/instances

go 1.16

replace gitlab.com/akita/mgpusim/v2 => ../../akita/mgpusim

require (
	github.com/tebeka/atexit v0.3.0
	gitlab.com/akita/akita/v2 v2.0.2
	gitlab.com/akita/mem/v2 v2.4.1
	gitlab.com/akita/mgpusim/v2 v2.0.0-00010101000000-000000000000
	gitlab.com/akita/noc/v2 v2.0.2
	gitlab.com/akita/util/v2 v2.1.0
)
