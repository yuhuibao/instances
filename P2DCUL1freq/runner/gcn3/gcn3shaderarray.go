package gcn3

import (
	"fmt"

	wriatearound "gitlab.com/akita/mem/v2/cache/writearound"
	"gitlab.com/akita/mem/v2/cache/writeevict"
	writethrough "gitlab.com/akita/mem/v2/cache/writethrough"
	"gitlab.com/akita/mgpusim/v2/timing/dcu"
	rob "gitlab.com/akita/mgpusim/v2/timing/rob"

	"gitlab.com/akita/akita/v2/sim"
	"gitlab.com/akita/mem/v2/mem"
	"gitlab.com/akita/mem/v2/vm/addresstranslator"
	"gitlab.com/akita/mem/v2/vm/tlb"
	"gitlab.com/akita/mgpusim/v2/isa"
	"gitlab.com/akita/util/v2/tracing"
)

type gcn3ShaderArray struct {
	cus []*dcu.ComputeUnit

	l0vROBs []*rob.ReorderBuffer
	l0sROBs []*rob.ReorderBuffer
	l0iROBs []*rob.ReorderBuffer

	l0vATs []*addresstranslator.AddressTranslator
	l0sATs []*addresstranslator.AddressTranslator
	l0iATs []*addresstranslator.AddressTranslator

	l0vCaches []*wriatearound.Cache
	l0sCaches []*writethrough.Cache
	l0iCaches []*writethrough.Cache

	l0vTLBs []*tlb.TLB
	l0sTLBs []*tlb.TLB
	l0iTLBs []*tlb.TLB

	l1Cache *writeevict.Cache
	l1Conn  *sim.DirectConnection
}

type gcn3ShaderArrayBuilder struct {
	gpuID uint64
	name  string
	numCU int

	engine            sim.Engine
	freq              sim.Freq
	isa               isa.ISA
	log2CacheLineSize uint64
	log2PageSize      uint64
	visTracer         tracing.Tracer
	memTracer         tracing.Tracer
}

func makeGCN3ShaderArrayBuilder() gcn3ShaderArrayBuilder {
	b := gcn3ShaderArrayBuilder{
		gpuID:             0,
		name:              "SA",
		numCU:             4,
		freq:              1 * sim.GHz,
		log2CacheLineSize: 6,
		log2PageSize:      12,
	}
	return b
}

func (b gcn3ShaderArrayBuilder) withEngine(e sim.Engine) gcn3ShaderArrayBuilder {
	b.engine = e
	return b
}

func (b gcn3ShaderArrayBuilder) withFreq(f sim.Freq) gcn3ShaderArrayBuilder {
	b.freq = f
	return b
}

func (b gcn3ShaderArrayBuilder) withISA(isa isa.ISA) gcn3ShaderArrayBuilder {
	b.isa = isa
	return b
}

func (b gcn3ShaderArrayBuilder) withGPUID(id uint64) gcn3ShaderArrayBuilder {
	b.gpuID = id
	return b
}

func (b gcn3ShaderArrayBuilder) withNumCU(n int) gcn3ShaderArrayBuilder {
	b.numCU = n
	return b
}

func (b gcn3ShaderArrayBuilder) withLog2CachelineSize(
	log2Size uint64,
) gcn3ShaderArrayBuilder {
	b.log2CacheLineSize = log2Size
	return b
}

func (b gcn3ShaderArrayBuilder) withLog2PageSize(
	log2Size uint64,
) gcn3ShaderArrayBuilder {
	b.log2PageSize = log2Size
	return b
}

func (b gcn3ShaderArrayBuilder) withVisTracer(
	visTracer tracing.Tracer,
) gcn3ShaderArrayBuilder {
	b.visTracer = visTracer
	return b
}

func (b gcn3ShaderArrayBuilder) withMemTracer(
	memTracer tracing.Tracer,
) gcn3ShaderArrayBuilder {
	b.memTracer = memTracer
	return b
}

func (b gcn3ShaderArrayBuilder) Build(name string) gcn3ShaderArray {
	b.name = name
	sa := gcn3ShaderArray{}

	b.buildComponents(&sa)
	b.connectComponents(&sa)

	return sa
}

func (b *gcn3ShaderArrayBuilder) buildComponents(sa *gcn3ShaderArray) {
	b.buildCUs(sa)

	b.buildL0VTLBs(sa)
	b.buildL0VAddressTranslators(sa)
	b.buildL0VReorderBuffers(sa)
	b.buildL0VCaches(sa)

	b.buildL0STLB(sa)
	b.buildL0SAddressTranslator(sa)
	b.buildL0SReorderBuffer(sa)
	b.buildL0SCache(sa)

	b.buildL0ITLB(sa)
	b.buildL0IAddressTranslator(sa)
	b.buildL0IReorderBuffer(sa)
	b.buildL0ICache(sa)

	b.buildL1Cache(sa)
}

func (b *gcn3ShaderArrayBuilder) connectComponents(sa *gcn3ShaderArray) {
	b.connectL1Cache(sa)
	b.connectVectorMem(sa)
	b.connectScalarMem(sa)
	b.connectInstMem(sa)
}

func (b *gcn3ShaderArrayBuilder) connectVectorMem(sa *gcn3ShaderArray) {
	for i := 0; i < b.numCU; i++ {
		cu := sa.cus[i]
		rob0 := sa.l0vROBs[i*2]
		rob1 := sa.l0vROBs[i*2+1]
		at0 := sa.l0vATs[i*2]
		at1 := sa.l0vATs[i*2+1]
		l0v0 := sa.l0vCaches[i*2]
		l0v1 := sa.l0vCaches[i*2+1]
		tlb0 := sa.l0vTLBs[i*2]
		tlb1 := sa.l0vTLBs[i*2+1]

		cu.VectorMemModules[0] = &mem.SingleLowModuleFinder{
			LowModule: rob0.GetPortByName("Top"),
		}
		cu.VectorMemModules[1] = &mem.SingleLowModuleFinder{
			LowModule: rob1.GetPortByName("Top"),
		}
		b.connect3WithDirectConnection(cu.ToVectorMem, rob0.GetPortByName("Top"), rob1.GetPortByName("Top"), 8)

		atTopPort0 := at0.GetPortByName("Top")
		rob0.BottomUnit = atTopPort0
		b.connectWithDirectConnection(
			rob0.GetPortByName("Bottom"), atTopPort0, 8)
		atTopPort1 := at1.GetPortByName("Top")
		rob1.BottomUnit = atTopPort1
		b.connectWithDirectConnection(
			rob1.GetPortByName("Bottom"), atTopPort1, 8)

		tlbTopPort0 := tlb0.GetPortByName("Top")
		at0.SetTranslationProvider(tlbTopPort0)
		b.connectWithDirectConnection(
			at0.GetPortByName("Translation"), tlbTopPort0, 8)
		tlbTopPort1 := tlb1.GetPortByName("Top")
		at1.SetTranslationProvider(tlbTopPort1)
		b.connectWithDirectConnection(
			at1.GetPortByName("Translation"), tlbTopPort1, 8)

		at0.SetLowModuleFinder(&mem.SingleLowModuleFinder{
			LowModule: l0v0.GetPortByName("Top"),
		})
		b.connectWithDirectConnection(l0v0.GetPortByName("Top"),
			at0.GetPortByName("Bottom"), 8)
		at1.SetLowModuleFinder(&mem.SingleLowModuleFinder{
			LowModule: l0v1.GetPortByName("Top"),
		})
		b.connectWithDirectConnection(l0v1.GetPortByName("Top"),
			at1.GetPortByName("Bottom"), 8)
	}
}

func (b *gcn3ShaderArrayBuilder) connectScalarMem(sa *gcn3ShaderArray) {
	for i := 0; i < b.numCU; i++ {
		cu := sa.cus[i]
		rob := sa.l0sROBs[i]
		at := sa.l0sATs[i]
		tlb := sa.l0sTLBs[i]
		l1s := sa.l0sCaches[i]

		cu.ScalarMem = rob.GetPortByName("Top")
		b.connectWithDirectConnection(cu.ToScalarMem,
			rob.GetPortByName("Top"), 8)

		atTopPort := at.GetPortByName("Top")
		rob.BottomUnit = atTopPort
		b.connectWithDirectConnection(rob.GetPortByName("Bottom"), atTopPort, 8)

		tlbTopPort := tlb.GetPortByName("Top")
		at.SetTranslationProvider(tlbTopPort)
		b.connectWithDirectConnection(
			at.GetPortByName("Translation"), tlbTopPort, 8)

		at.SetLowModuleFinder(&mem.SingleLowModuleFinder{
			LowModule: l1s.GetPortByName("Top"),
		})
		b.connectWithDirectConnection(
			l1s.GetPortByName("Top"), at.GetPortByName("Bottom"), 8)
	}
}

func (b *gcn3ShaderArrayBuilder) connectInstMem(sa *gcn3ShaderArray) {
	for i := 0; i < b.numCU; i++ {
		cu := sa.cus[i]
		rob := sa.l0iROBs[i]
		at := sa.l0iATs[i]
		tlb := sa.l0iTLBs[i]
		l1i := sa.l0iCaches[i]

		cu.InstMem = rob.GetPortByName("Top")
		b.connectWithDirectConnection(cu.ToInstMem,
			rob.GetPortByName("Top"), 8)
		l1iTopPort := l1i.GetPortByName("Top")
		rob.BottomUnit = l1iTopPort
		b.connectWithDirectConnection(rob.GetPortByName("Bottom"), l1iTopPort, 8)

		atTopPort := at.GetPortByName("Top")
		l1i.SetLowModuleFinder(&mem.SingleLowModuleFinder{
			LowModule: atTopPort,
		})
		b.connectWithDirectConnection(l1i.GetPortByName("Bottom"), atTopPort, 8)

		tlbTopPort := tlb.GetPortByName("Top")
		at.SetTranslationProvider(tlbTopPort)
		b.connectWithDirectConnection(
			at.GetPortByName("Translation"), tlbTopPort, 8)
	}
}

func (b *gcn3ShaderArrayBuilder) connectL1Cache(sa *gcn3ShaderArray) {
	l1TopPort := sa.l1Cache.GetPortByName("Top")
	l1ConnName := fmt.Sprintf("%s.L1CacheConn", b.name)
	l1Conn := sim.NewDirectConnection(l1ConnName, b.engine, b.freq)
	sa.l1Conn = l1Conn
	l1Conn.PlugIn(l1TopPort, 8)

	for _, at := range sa.l0iATs {
		at.SetLowModuleFinder(&mem.SingleLowModuleFinder{
			LowModule: l1TopPort,
		})
		l1Conn.PlugIn(at.GetPortByName("Bottom"), 8)
	}

	for _, cache := range sa.l0sCaches {
		cache.SetLowModuleFinder(&mem.SingleLowModuleFinder{
			LowModule: l1TopPort,
		})
		l1Conn.PlugIn(cache.GetPortByName("Bottom"), 8)
	}

	for _, cache := range sa.l0vCaches {
		cache.SetLowModuleFinder(&mem.SingleLowModuleFinder{
			LowModule: l1TopPort,
		})
		l1Conn.PlugIn(cache.GetPortByName("Bottom"), 8)
	}
}
func (b *gcn3ShaderArrayBuilder) connectWithDirectConnection(
	port1, port2 sim.Port,
	bufferSize int,
) {
	name := fmt.Sprintf("%s-%s", port1.Name(), port2.Name())
	conn := sim.NewDirectConnection(
		name,
		b.engine, b.freq,
	)
	conn.PlugIn(port1, bufferSize)
	conn.PlugIn(port2, bufferSize)
}

func (b *gcn3ShaderArrayBuilder) connect3WithDirectConnection(
	port1, port2, port3 sim.Port,
	bufferSize int,
) {
	name := fmt.Sprintf("%s-%s-%s", port1.Name(), port2.Name(), port3.Name())
	conn := sim.NewDirectConnection(
		name,
		b.engine, b.freq,
	)
	conn.PlugIn(port1, bufferSize)
	conn.PlugIn(port2, bufferSize)
	conn.PlugIn(port3, bufferSize)
}
func (b *gcn3ShaderArrayBuilder) buildCUs(sa *gcn3ShaderArray) {
	cuBuilder := dcu.MakeBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithLog2CachelineSize(b.log2CacheLineSize).
		WithISA(b.isa).
		WithVectorMemPipelineLatency(110)

	for i := 0; i < b.numCU; i++ {
		cuName := fmt.Sprintf("%s.CU_%02d", b.name, i)
		cu := cuBuilder.Build(cuName)
		sa.cus = append(sa.cus, cu)

		if b.visTracer != nil {
			tracing.CollectTrace(cu, b.visTracer)
		}
	}
}

func (b *gcn3ShaderArrayBuilder) buildL0VReorderBuffers(sa *gcn3ShaderArray) {
	builder := rob.MakeBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithBufferSize(128).
		WithNumReqPerCycle(4)

	for i := 0; i < b.numCU; i++ {
		name := fmt.Sprintf("%s.L0VROB_%02d_00", b.name, i)
		rob0 := builder.Build(name)
		sa.l0vROBs = append(sa.l0vROBs, rob0)

		if b.visTracer != nil {
			tracing.CollectTrace(rob0, b.visTracer)
		}
		name = fmt.Sprintf("%s.L0VROB_%02d_01", b.name, i)
		rob1 := builder.Build(name)
		sa.l0vROBs = append(sa.l0vROBs, rob1)

		if b.visTracer != nil {
			tracing.CollectTrace(rob1, b.visTracer)
		}
	}
}

func (b *gcn3ShaderArrayBuilder) buildL0VAddressTranslators(sa *gcn3ShaderArray) {
	builder := addresstranslator.MakeBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithDeviceID(b.gpuID).
		WithLog2PageSize(b.log2PageSize)

	for i := 0; i < b.numCU; i++ {
		name := fmt.Sprintf("%s.L0VAddrTrans_%02d_00", b.name, i)
		at0 := builder.Build(name)
		sa.l0vATs = append(sa.l0vATs, at0)

		if b.visTracer != nil {
			tracing.CollectTrace(at0, b.visTracer)
		}
		name = fmt.Sprintf("%s.L0VAddrTrans_%02d_01", b.name, i)
		at1 := builder.Build(name)
		sa.l0vATs = append(sa.l0vATs, at1)

		if b.visTracer != nil {
			tracing.CollectTrace(at1, b.visTracer)
		}
	}
}

func (b *gcn3ShaderArrayBuilder) buildL0VTLBs(sa *gcn3ShaderArray) {
	builder := tlb.MakeBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithNumMSHREntry(4).
		WithNumSets(1).
		WithNumWays(64).
		WithNumReqPerCycle(4)

	for i := 0; i < b.numCU; i++ {
		name := fmt.Sprintf("%s.L0VTLB_%02d_00", b.name, i)
		tlb0 := builder.Build(name)
		sa.l0vTLBs = append(sa.l0vTLBs, tlb0)

		if b.visTracer != nil {
			tracing.CollectTrace(tlb0, b.visTracer)
		}
		name = fmt.Sprintf("%s.L0VTLB_%02d_01", b.name, i)
		tlb1 := builder.Build(name)
		sa.l0vTLBs = append(sa.l0vTLBs, tlb1)

		if b.visTracer != nil {
			tracing.CollectTrace(tlb1, b.visTracer)
		}
	}
}

func (b *gcn3ShaderArrayBuilder) buildL0VCaches(sa *gcn3ShaderArray) {
	builder := wriatearound.NewBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithNumBanks(1).
		WithLog2BlockSize(b.log2CacheLineSize).
		WithWayAssociativity(4).
		WithNumMSHREntry(16).
		WithTotalByteSize(16 * mem.KB).
		WithDirectoryLatency(2).
		WithBankLatency(5)

	if b.visTracer != nil {
		builder = builder.WithVisTracer(b.visTracer)
	}

	for i := 0; i < b.numCU; i++ {
		name := fmt.Sprintf("%s.L0VCache_%02d_00", b.name, i)
		cache0 := builder.Build(name)
		sa.l0vCaches = append(sa.l0vCaches, cache0)

		if b.memTracer != nil {
			tracing.CollectTrace(cache0, b.memTracer)
		}
		name = fmt.Sprintf("%s.L0VCache_%02d_01", b.name, i)
		cache1 := builder.Build(name)
		sa.l0vCaches = append(sa.l0vCaches, cache1)

		if b.memTracer != nil {
			tracing.CollectTrace(cache1, b.memTracer)
		}
	}
}

func (b *gcn3ShaderArrayBuilder) buildL0SReorderBuffer(sa *gcn3ShaderArray) {
	builder := rob.MakeBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithBufferSize(128).
		WithNumReqPerCycle(4)
	for i := 0; i < b.numCU; i++ {
		name := fmt.Sprintf("%s.L0SROB_%02d", b.name, i)
		rob := builder.Build(name)
		sa.l0sROBs = append(sa.l0sROBs, rob)

		if b.visTracer != nil {
			tracing.CollectTrace(rob, b.visTracer)
		}
	}
}

func (b *gcn3ShaderArrayBuilder) buildL0SAddressTranslator(sa *gcn3ShaderArray) {
	builder := addresstranslator.MakeBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithDeviceID(b.gpuID).
		WithLog2PageSize(b.log2PageSize)
	for i := 0; i < b.numCU; i++ {
		name := fmt.Sprintf("%s.L0SAddrTrans_%02d", b.name, i)
		at := builder.Build(name)
		sa.l0sATs = append(sa.l0sATs, at)

		if b.visTracer != nil {
			tracing.CollectTrace(at, b.visTracer)
		}
	}
}

func (b *gcn3ShaderArrayBuilder) buildL0STLB(sa *gcn3ShaderArray) {
	builder := tlb.MakeBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithNumMSHREntry(4).
		WithNumSets(1).
		WithNumWays(64).
		WithNumReqPerCycle(4)
	for i := 0; i < b.numCU; i++ {
		name := fmt.Sprintf("%s.L0STLB_%02d", b.name, i)
		tlb := builder.Build(name)
		sa.l0sTLBs = append(sa.l0sTLBs, tlb)

		if b.visTracer != nil {
			tracing.CollectTrace(tlb, b.visTracer)
		}
	}
}

func (b *gcn3ShaderArrayBuilder) buildL0SCache(sa *gcn3ShaderArray) {
	builder := writethrough.NewBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithBankLatency(1).
		WithNumBanks(1).
		WithLog2BlockSize(b.log2CacheLineSize).
		WithWayAssociativity(4).
		WithNumMSHREntry(16).
		WithTotalByteSize(16 * mem.KB)
	for i := 0; i < b.numCU; i++ {
		name := fmt.Sprintf("%s.L0SCache_%02d", b.name, i)
		cache := builder.Build(name)
		sa.l0sCaches = append(sa.l0sCaches, cache)

		if b.visTracer != nil {
			tracing.CollectTrace(cache, b.visTracer)
		}
		if b.memTracer != nil {
			tracing.CollectTrace(cache, b.memTracer)
		}
	}
}

func (b *gcn3ShaderArrayBuilder) buildL0IReorderBuffer(sa *gcn3ShaderArray) {
	builder := rob.MakeBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithBufferSize(128).
		WithNumReqPerCycle(4)
	for i := 0; i < b.numCU; i++ {
		name := fmt.Sprintf("%s.L0IROB_%02d", b.name, i)
		rob := builder.Build(name)
		sa.l0iROBs = append(sa.l0iROBs, rob)

		if b.visTracer != nil {
			tracing.CollectTrace(rob, b.visTracer)
		}
	}
}

func (b *gcn3ShaderArrayBuilder) buildL0IAddressTranslator(sa *gcn3ShaderArray) {
	builder := addresstranslator.MakeBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithDeviceID(b.gpuID).
		WithLog2PageSize(b.log2PageSize)
	for i := 0; i < b.numCU; i++ {
		name := fmt.Sprintf("%s.L0IAddrTrans_%02d", b.name, i)
		at := builder.Build(name)
		sa.l0iATs = append(sa.l0iATs, at)

		if b.visTracer != nil {
			tracing.CollectTrace(at, b.visTracer)
		}
	}
}

func (b *gcn3ShaderArrayBuilder) buildL0ITLB(sa *gcn3ShaderArray) {
	builder := tlb.MakeBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithNumMSHREntry(4).
		WithNumSets(1).
		WithNumWays(64).
		WithNumReqPerCycle(4)
	for i := 0; i < b.numCU; i++ {
		name := fmt.Sprintf("%s.L0ITLB_%02d", b.name, i)
		tlb := builder.Build(name)
		sa.l0iTLBs = append(sa.l0iTLBs, tlb)

		if b.visTracer != nil {
			tracing.CollectTrace(tlb, b.visTracer)
		}
	}
}

func (b *gcn3ShaderArrayBuilder) buildL0ICache(sa *gcn3ShaderArray) {
	builder := writethrough.NewBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithBankLatency(1).
		WithNumBanks(1).
		WithLog2BlockSize(b.log2CacheLineSize).
		WithWayAssociativity(4).
		WithNumMSHREntry(16).
		WithTotalByteSize(32 * mem.KB).
		WithNumReqsPerCycle(4)
	for i := 0; i < b.numCU; i++ {
		name := fmt.Sprintf("%s.L0ICache_%02d", b.name, i)
		cache := builder.Build(name)
		sa.l0iCaches = append(sa.l0iCaches, cache)

		if b.visTracer != nil {
			tracing.CollectTrace(cache, b.visTracer)
		}

		if b.memTracer != nil {
			tracing.CollectTrace(cache, b.memTracer)
		}
	}
}
func (b *gcn3ShaderArrayBuilder) buildL1Cache(sa *gcn3ShaderArray) {
	builder := writeevict.NewBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithDirectoryLatency(12).
		WithBankLatency(45).
		WithNumBanks(1).
		WithLog2BlockSize(b.log2CacheLineSize).
		WithWayAssociativity(16).
		WithNumMSHREntry(1024).
		WithTotalByteSize(128 * mem.KB).
		WithNumReqsPerCycle(16).
		WithMaxNumConcurrentTrans(1024)

	name := fmt.Sprintf("%s.L1Cache", b.name)
	cache := builder.Build(name)
	sa.l1Cache = cache

	if b.visTracer != nil {
		tracing.CollectTrace(cache, b.visTracer)
	}

	if b.memTracer != nil {
		tracing.CollectTrace(cache, b.memTracer)
	}
}
