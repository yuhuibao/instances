// Package cdna defines how a CDNA GPU is configured.
package cdna

import (
	"fmt"
	"log"
	"os"

	"gitlab.com/akita/mgpusim/v2/timing/rob"

	"gitlab.com/akita/akita/v2/monitoring"
	"gitlab.com/akita/akita/v2/sim"
	"gitlab.com/akita/mem/v2/cache/writearound"
	"gitlab.com/akita/mem/v2/cache/writeback"
	"gitlab.com/akita/mem/v2/cache/writethrough"
	"gitlab.com/akita/mem/v2/idealmemcontroller"
	"gitlab.com/akita/mem/v2/mem"
	"gitlab.com/akita/mem/v2/vm/addresstranslator"
	"gitlab.com/akita/mem/v2/vm/mmu"
	"gitlab.com/akita/mem/v2/vm/tlb"
	"gitlab.com/akita/mgpusim/v2/isa"
	"gitlab.com/akita/mgpusim/v2/isa/cdna"
	"gitlab.com/akita/mgpusim/v2/pagemigrationcontroller"
	"gitlab.com/akita/mgpusim/v2/rdma"
	"gitlab.com/akita/mgpusim/v2/timing/cp"
	"gitlab.com/akita/mgpusim/v2/timing/cu"
	"gitlab.com/akita/util/v2/tracing"
	"gitlab.com/yuhuibao/instances/P2DCU/runner/platform"
)

// GPUBuilder can build AMD CDNA GPUs.
type GPUBuilder struct {
	engine                         sim.Engine
	freq                           sim.Freq
	isa                            isa.ISA
	memAddrOffset                  uint64
	mmu                            *mmu.MMU
	numShaderArray                 int
	numCUPerShaderArray            int
	numMemoryBank                  int
	l2CacheSize                    uint64
	dramSize                       uint64
	log2PageSize                   uint64
	log2CacheLineSize              uint64
	log2MemoryBankInterleavingSize uint64

	enableISADebugging bool
	enableMemTracing   bool
	enableVisTracing   bool
	visTracer          tracing.Tracer
	memTracer          tracing.Tracer
	monitor            *monitoring.Monitor

	gpuName                 string
	gpu                     *platform.GPU
	gpuID                   uint64
	cp                      *cp.CommandProcessor
	latencyTable            []int
	constantKernelOverhead  int
	firstLaunchOverhead     int
	cus                     []*cu.ComputeUnit
	l1vReorderBuffers       []*rob.ReorderBuffer
	l1iReorderBuffers       []*rob.ReorderBuffer
	l1sReorderBuffers       []*rob.ReorderBuffer
	l1vCaches               []*writearound.Cache
	l1sCaches               []*writethrough.Cache
	l1iCaches               []*writethrough.Cache
	l2Caches                []*writeback.Cache
	l1vAddrTrans            []*addresstranslator.AddressTranslator
	l1sAddrTrans            []*addresstranslator.AddressTranslator
	l1iAddrTrans            []*addresstranslator.AddressTranslator
	l1vTLBs                 []*tlb.TLB
	l1sTLBs                 []*tlb.TLB
	l1iTLBs                 []*tlb.TLB
	l2TLBs                  []*tlb.TLB
	drams                   []sim.Component
	lowModuleFinderForL1    *mem.InterleavedLowModuleFinder
	lowModuleFinderForL2    *mem.InterleavedLowModuleFinder
	lowModuleFinderForPMC   *mem.InterleavedLowModuleFinder
	dmaEngine               *cp.DMAEngine
	rdmaEngine              *rdma.Engine
	pageMigrationController *pagemigrationcontroller.PageMigrationController

	internalConn           *sim.DirectConnection
	l1TLBToL2TLBConnection *sim.DirectConnection
	l1ToL2Connection       *sim.DirectConnection
	l2ToDramConnection     *sim.DirectConnection
}

// MakeGPUBuilder provides a GPU builder that can builds the R9Nano GPU.
func MakeGPUBuilder() GPUBuilder {
	b := GPUBuilder{
		freq:                           1500 * sim.MHz,
		isa:                            cdna.ISA{},
		numShaderArray:                 16,
		numCUPerShaderArray:            4,
		numMemoryBank:                  16,
		log2CacheLineSize:              6,
		log2PageSize:                   12,
		log2MemoryBankInterleavingSize: 7,
		l2CacheSize:                    8 * mem.MB,
		dramSize:                       4 * mem.GB,
	}
	return b
}

// WithEngine sets the engine that the GPU use.
func (b GPUBuilder) WithEngine(engine sim.Engine) GPUBuilder {
	b.engine = engine
	return b
}

// WithFreq sets the frequency that the GPU works at.
func (b GPUBuilder) WithFreq(freq sim.Freq) GPUBuilder {
	b.freq = freq
	return b
}

// WithLatencyTable sets latencyTable
func (b GPUBuilder) WithLatencyTable(latencyTable []int) GPUBuilder {
	b.latencyTable = latencyTable
	return b
}

// WithConstantKernelOverhead sets latencyTable
func (b GPUBuilder) WithConstantKernelOverhead(constantKernelOverhead int) GPUBuilder {
	b.constantKernelOverhead = constantKernelOverhead
	return b
}

// WithFirstLaunchOverhead sets firstLaunchOverhead
func (b GPUBuilder) WithFirstLaunchOverhead(firstLaunchOverhead int) GPUBuilder {
	b.firstLaunchOverhead = firstLaunchOverhead
	return b
}

// WithISA sets the ISA that the GPUs to use.
func (b GPUBuilder) WithISA(isa isa.ISA) GPUBuilder {
	b.isa = isa
	return b
}

// WithMemAddrOffset sets the address of the first byte of the GPU to build.
func (b GPUBuilder) WithMemAddrOffset(
	offset uint64,
) GPUBuilder {
	b.memAddrOffset = offset
	return b
}

// WithMMU sets the MMU component that provides the address translation service
// for the GPU.
func (b GPUBuilder) WithMMU(mmu *mmu.MMU) GPUBuilder {
	b.mmu = mmu
	return b
}

// WithNumMemoryBank sets the number of L2 cache modules and number of memory
// controllers in each GPU.
func (b GPUBuilder) WithNumMemoryBank(n int) GPUBuilder {
	b.numMemoryBank = n
	return b
}

// WithNumShaderArray sets the number of shader arrays in each GPU. Each shader
// array contains a certain number of CUs, a certain number of L1V caches, 1
// L1S cache, and 1 L1V cache.
func (b GPUBuilder) WithNumShaderArray(n int) GPUBuilder {
	b.numShaderArray = n
	return b
}

// WithNumCUPerShaderArray sets the number of CU and number of L1V caches in
// each Shader Array.
func (b GPUBuilder) WithNumCUPerShaderArray(n int) GPUBuilder {
	b.numCUPerShaderArray = n
	return b
}

// WithLog2MemoryBankInterleavingSize sets the number of consecutive bytes that
// are guaranteed to be on a memory bank.
func (b GPUBuilder) WithLog2MemoryBankInterleavingSize(
	n uint64,
) GPUBuilder {
	b.log2MemoryBankInterleavingSize = n
	return b
}

// WithVisTracer applies a tracer to trace all the tasks of all the GPU
// components
func (b GPUBuilder) WithVisTracer(t tracing.Tracer) GPUBuilder {
	b.enableVisTracing = true
	b.visTracer = t
	return b
}

// WithMemTracer applies a tracer to trace the memory transactions.
func (b GPUBuilder) WithMemTracer(t tracing.Tracer) GPUBuilder {
	b.enableMemTracing = true
	b.memTracer = t
	return b
}

// WithISADebugging enables the GPU to dump instruction execution information.
func (b GPUBuilder) WithISADebugging() GPUBuilder {
	b.enableISADebugging = true
	return b
}

// WithLog2CacheLineSize sets the cache line size with the power of 2.
func (b GPUBuilder) WithLog2CacheLineSize(
	log2CacheLine uint64,
) GPUBuilder {
	b.log2CacheLineSize = log2CacheLine
	return b
}

// WithLog2PageSize sets the page size with the power of 2.
func (b GPUBuilder) WithLog2PageSize(log2PageSize uint64) GPUBuilder {
	b.log2PageSize = log2PageSize
	return b
}

// WithMonitor sets the monitor to use.
func (b GPUBuilder) WithMonitor(m *monitoring.Monitor) GPUBuilder {
	b.monitor = m
	return b
}

// WithL2CacheSize set the total L2 cache size. The size of the L2 cache is
// split between memory banks.
func (b GPUBuilder) WithL2CacheSize(size uint64) GPUBuilder {
	b.l2CacheSize = size
	return b
}

// WithDRAMSize sets the total byte size of DRAM.
func (b GPUBuilder) WithDRAMSize(size uint64) GPUBuilder {
	b.dramSize = size
	return b
}

// Build creates a pre-configure GPU similar to the AMD R9 Nano GPU.
func (b GPUBuilder) Build(name string, id uint64) *platform.GPU {
	b.createGPU(name, id)
	b.buildSAs()
	b.buildL2Caches()
	b.buildDRAMControllers()
	b.buildCP()
	b.buildL2TLB()

	b.connectCP()
	b.connectL2AndDRAM()
	b.connectL1ToL2()
	b.connectL1TLBToL2TLB()

	b.populateExternalPorts()

	return b.gpu
}

func (b *GPUBuilder) populateExternalPorts() {
	b.gpu.Domain.AddPort("CommandProcessor", b.cp.ToDriver)
	b.gpu.Domain.AddPort("RDMA", b.rdmaEngine.ToOutside)
	b.gpu.Domain.AddPort("PageMigrationController",
		b.pageMigrationController.GetPortByName("Remote"))

	for i, l2TLB := range b.l2TLBs {
		name := fmt.Sprintf("Translation_%02d", i)
		b.gpu.Domain.AddPort(name, l2TLB.GetPortByName("Bottom"))
	}
}

func (b *GPUBuilder) createGPU(name string, id uint64) {
	b.gpuName = name

	b.gpu = &platform.GPU{}
	b.gpu.Domain = sim.NewDomain(b.gpuName)
	b.gpuID = id
}

func (b *GPUBuilder) connectCP() {
	b.internalConn = sim.NewDirectConnection(
		b.gpuName+"InternalConn", b.engine, b.freq)

	b.internalConn.PlugIn(b.cp.ToDriver, 1)
	b.internalConn.PlugIn(b.cp.ToDMA, 128)
	b.internalConn.PlugIn(b.cp.ToCaches, 128)
	b.internalConn.PlugIn(b.cp.ToCUs, 128)
	b.internalConn.PlugIn(b.cp.ToTLBs, 128)
	b.internalConn.PlugIn(b.cp.ToAddressTranslators, 128)
	b.internalConn.PlugIn(b.cp.ToRDMA, 4)
	b.internalConn.PlugIn(b.cp.ToPMC, 4)

	b.cp.RDMA = b.rdmaEngine.CtrlPort
	b.internalConn.PlugIn(b.cp.RDMA, 1)

	b.cp.DMAEngine = b.dmaEngine.ToCP
	b.internalConn.PlugIn(b.dmaEngine.ToCP, 1)

	pmcControlPort := b.pageMigrationController.GetPortByName("Control")
	b.cp.PMC = pmcControlPort
	b.internalConn.PlugIn(pmcControlPort, 1)

	b.connectCPWithCUs()
	b.connectCPWithAddressTranslators()
	b.connectCPWithTLBs()
	b.connectCPWithCaches()
}

func (b *GPUBuilder) connectL1ToL2() {
	lowModuleFinder := mem.NewInterleavedLowModuleFinder(
		1 << b.log2MemoryBankInterleavingSize)
	lowModuleFinder.ModuleForOtherAddresses = b.rdmaEngine.ToL1
	lowModuleFinder.UseAddressSpaceLimitation = true
	lowModuleFinder.LowAddress = b.memAddrOffset
	lowModuleFinder.HighAddress = b.memAddrOffset + 4*mem.GB

	l1ToL2Conn := sim.NewDirectConnection(b.gpuName+".L1-L2",
		b.engine, b.freq)

	b.rdmaEngine.SetLocalModuleFinder(lowModuleFinder)
	l1ToL2Conn.PlugIn(b.rdmaEngine.ToL1, 64)
	l1ToL2Conn.PlugIn(b.rdmaEngine.ToL2, 64)

	for _, l2 := range b.l2Caches {
		lowModuleFinder.LowModules = append(lowModuleFinder.LowModules,
			l2.GetPortByName("Top"))
		l1ToL2Conn.PlugIn(l2.GetPortByName("Top"), 64)
	}

	for _, l1vAT := range b.l1vAddrTrans {
		l1vAT.SetLowModuleFinder(lowModuleFinder)
		l1ToL2Conn.PlugIn(l1vAT.GetPortByName("Bottom"), 16)
	}

	for _, l1s := range b.l1sCaches {
		l1s.SetLowModuleFinder(lowModuleFinder)
		l1ToL2Conn.PlugIn(l1s.GetPortByName("Bottom"), 16)
	}

	for _, l1iAT := range b.l1iAddrTrans {
		l1iAT.SetLowModuleFinder(lowModuleFinder)
		l1ToL2Conn.PlugIn(l1iAT.GetPortByName("Bottom"), 16)
	}
}

func (b *GPUBuilder) connectL2AndDRAM() {
	b.l2ToDramConnection = sim.NewDirectConnection(
		b.gpuName+"L2-DRAM", b.engine, b.freq)

	lowModuleFinder := mem.NewInterleavedLowModuleFinder(
		1 << b.log2MemoryBankInterleavingSize)

	for i, l2 := range b.l2Caches {
		b.l2ToDramConnection.PlugIn(l2.GetPortByName("Bottom"), 64)
		l2.SetLowModuleFinder(&mem.SingleLowModuleFinder{
			LowModule: b.drams[i].GetPortByName("Top"),
		})
	}

	for _, dram := range b.drams {
		b.l2ToDramConnection.PlugIn(dram.GetPortByName("Top"), 64)
		lowModuleFinder.LowModules = append(lowModuleFinder.LowModules,
			dram.GetPortByName("Top"))
	}

	b.dmaEngine.SetLocalDataSource(lowModuleFinder)
	b.l2ToDramConnection.PlugIn(b.dmaEngine.ToMem, 64)

	b.pageMigrationController.MemCtrlFinder = lowModuleFinder
	b.l2ToDramConnection.PlugIn(
		b.pageMigrationController.GetPortByName("LocalMem"), 16)
}

func (b *GPUBuilder) connectL1TLBToL2TLB() {
	tlbConn := sim.NewDirectConnection(b.gpuName+"L1TLB-L2TLB",
		b.engine, b.freq)

	tlbConn.PlugIn(b.l2TLBs[0].GetPortByName("Top"), 64)

	for _, l1vTLB := range b.l1vTLBs {
		l1vTLB.LowModule = b.l2TLBs[0].GetPortByName("Top")
		tlbConn.PlugIn(l1vTLB.GetPortByName("Bottom"), 16)
	}

	for _, l1iTLB := range b.l1iTLBs {
		l1iTLB.LowModule = b.l2TLBs[0].GetPortByName("Top")
		tlbConn.PlugIn(l1iTLB.GetPortByName("Bottom"), 16)
	}

	for _, l1sTLB := range b.l1sTLBs {
		l1sTLB.LowModule = b.l2TLBs[0].GetPortByName("Top")
		tlbConn.PlugIn(l1sTLB.GetPortByName("Bottom"), 16)
	}
}

func (b *GPUBuilder) connectCPWithCUs() {
	for _, cu := range b.cus {
		b.cp.RegisterCU(cu)
		b.internalConn.PlugIn(cu.ToACE, 1)
		b.internalConn.PlugIn(cu.ToCP, 1)
	}
}

func (b *GPUBuilder) connectCPWithAddressTranslators() {
	for _, at := range b.l1vAddrTrans {
		ctrlPort := at.GetPortByName("Control")
		b.cp.AddressTranslators = append(b.cp.AddressTranslators, ctrlPort)
		b.internalConn.PlugIn(ctrlPort, 1)
	}

	for _, at := range b.l1sAddrTrans {
		ctrlPort := at.GetPortByName("Control")
		b.cp.AddressTranslators = append(b.cp.AddressTranslators, ctrlPort)
		b.internalConn.PlugIn(ctrlPort, 1)
	}

	for _, at := range b.l1iAddrTrans {
		ctrlPort := at.GetPortByName("Control")
		b.cp.AddressTranslators = append(b.cp.AddressTranslators, ctrlPort)
		b.internalConn.PlugIn(ctrlPort, 1)
	}

	for _, rob := range b.l1vReorderBuffers {
		ctrlPort := rob.GetPortByName("Control")
		b.cp.AddressTranslators = append(
			b.cp.AddressTranslators, ctrlPort)
		b.internalConn.PlugIn(ctrlPort, 1)
	}

	for _, rob := range b.l1iReorderBuffers {
		ctrlPort := rob.GetPortByName("Control")
		b.cp.AddressTranslators = append(
			b.cp.AddressTranslators, ctrlPort)
		b.internalConn.PlugIn(ctrlPort, 1)
	}

	for _, rob := range b.l1sReorderBuffers {
		ctrlPort := rob.GetPortByName("Control")
		b.cp.AddressTranslators = append(
			b.cp.AddressTranslators, ctrlPort)
		b.internalConn.PlugIn(ctrlPort, 1)
	}
}

func (b *GPUBuilder) connectCPWithTLBs() {
	for _, tlb := range b.l2TLBs {
		ctrlPort := tlb.GetPortByName("Control")
		b.cp.TLBs = append(b.cp.TLBs, ctrlPort)
		b.internalConn.PlugIn(ctrlPort, 1)
	}

	for _, tlb := range b.l1vTLBs {
		ctrlPort := tlb.GetPortByName("Control")
		b.cp.TLBs = append(b.cp.TLBs, ctrlPort)
		b.internalConn.PlugIn(ctrlPort, 1)
	}

	for _, tlb := range b.l1sTLBs {
		ctrlPort := tlb.GetPortByName("Control")
		b.cp.TLBs = append(b.cp.TLBs, ctrlPort)
		b.internalConn.PlugIn(ctrlPort, 1)
	}

	for _, tlb := range b.l1iTLBs {
		ctrlPort := tlb.GetPortByName("Control")
		b.cp.TLBs = append(b.cp.TLBs, ctrlPort)
		b.internalConn.PlugIn(ctrlPort, 1)
	}
}

func (b *GPUBuilder) connectCPWithCaches() {
	for _, c := range b.l1iCaches {
		ctrlPort := c.GetPortByName("Control")
		b.cp.L1ICaches = append(b.cp.L1ICaches, ctrlPort)
		b.internalConn.PlugIn(ctrlPort, 1)
	}

	for _, c := range b.l1vCaches {
		ctrlPort := c.GetPortByName("Control")
		b.cp.L1VCaches = append(b.cp.L1VCaches, ctrlPort)
		b.internalConn.PlugIn(ctrlPort, 1)
	}

	for _, c := range b.l1sCaches {
		ctrlPort := c.GetPortByName("Control")
		b.cp.L1SCaches = append(b.cp.L1SCaches, ctrlPort)
		b.internalConn.PlugIn(ctrlPort, 1)
	}

	for _, c := range b.l2Caches {
		ctrlPort := c.GetPortByName("Control")
		b.cp.L2Caches = append(b.cp.L2Caches, ctrlPort)
		b.internalConn.PlugIn(ctrlPort, 1)
	}
}

func (b *GPUBuilder) buildSAs() {
	saBuilder := makeCDNAShaderArrayBuilder().
		withEngine(b.engine).
		withFreq(b.freq).
		withGPUID(b.gpuID).
		withISA(b.isa).
		withLog2CachelineSize(b.log2CacheLineSize).
		withLog2PageSize(b.log2PageSize).
		withNumCU(b.numCUPerShaderArray)

	if b.enableVisTracing {
		saBuilder = saBuilder.withVisTracer(b.visTracer)
	}

	if b.enableMemTracing {
		saBuilder = saBuilder.withMemTracer(b.memTracer)
	}

	for i := 0; i < b.numShaderArray; i++ {
		saName := fmt.Sprintf("%s.SA_%02d", b.gpuName, i)
		b.buildSA(saBuilder, saName)
	}
}

func (b *GPUBuilder) buildL2Caches() {
	byteSize := b.l2CacheSize / uint64(b.numMemoryBank)
	l2Builder := writeback.MakeBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithLog2BlockSize(b.log2CacheLineSize).
		WithWayAssociativity(16).
		WithByteSize(byteSize).
		WithNumMSHREntry(64).
		WithNumReqPerCycle(1).
		WithDirectoryLatency(40). //original 100
		WithBankLatency(4)        //original 40

	for i := 0; i < b.numMemoryBank; i++ {
		cacheName := fmt.Sprintf("%s.L2_%d", b.gpuName, i)
		l2 := l2Builder.
			WithInterleaving(
				1<<(b.log2MemoryBankInterleavingSize-b.log2CacheLineSize),
				b.numMemoryBank,
				i,
			).Build(cacheName)
		b.l2Caches = append(b.l2Caches, l2)
		b.gpu.Caches = append(b.gpu.Caches, l2)

		if b.enableVisTracing {
			tracing.CollectTrace(l2, b.visTracer)
		}

		if b.enableMemTracing {
			tracing.CollectTrace(l2, b.memTracer)
		}

		if b.monitor != nil {
			b.monitor.RegisterComponent(l2)
		}
	}
}

func (b *GPUBuilder) buildDRAMControllers() {
	for i := 0; i < b.numMemoryBank; i++ {
		dramName := fmt.Sprintf("%s.DRAM_%d", b.gpuName, i)
		dram := idealmemcontroller.New(
			dramName, b.engine, b.dramSize/uint64(b.numMemoryBank))
		dram.AddressConverter = mem.InterleavingConverter{
			InterleavingSize:    1 << b.log2MemoryBankInterleavingSize,
			TotalNumOfElements:  b.numMemoryBank,
			CurrentElementIndex: i,
			Offset:              b.memAddrOffset,
		}
		dram.Latency = 12 //original 120
		dram.MaxNumTransaction = 100

		b.drams = append(b.drams, dram)
		b.gpu.MemControllers = append(b.gpu.MemControllers, dram)

		if b.enableVisTracing {
			tracing.CollectTrace(dram, b.visTracer)
		}

		if b.enableMemTracing {
			tracing.CollectTrace(dram, b.memTracer)
		}

		if b.monitor != nil {
			b.monitor.RegisterComponent(dram)
		}
	}
}

// func (b *GPUBuilder) buildDRAMControllers() {
// 	memCtrlBuilder := b.createDramControllerBuilder()

// 	for i := 0; i < b.numMemoryBank; i++ {
// 		dramName := fmt.Sprintf("%s.DRAM_%d", b.gpuName, i)
// 		dram := memCtrlBuilder.
// 			WithInterleavingAddrConversion(
// 				1<<b.log2MemoryBankInterleavingSize,
// 				b.numMemoryBank,
// 				i, b.memAddrOffset, b.memAddrOffset+4*mem.GB,
// 			).
// 			Build(dramName)
// 		// dram := idealmemcontroller.New(
// 		// 	fmt.Sprintf("%s.DRAM_%d", b.gpuName, i),
// 		// 	b.engine, 512*mem.MB)
// 		b.drams = append(b.drams, dram)
// 		b.gpu.MemControllers = append(b.gpu.MemControllers, dram)

// 		if b.enableVisTracing {
// 			tracing.CollectTrace(dram, b.visTracer)
// 		}

// 		if b.enableMemTracing {
// 			tracing.CollectTrace(dram, b.memTracer)
// 		}

// 		if b.monitor != nil {
// 			b.monitor.RegisterComponent(dram)
// 		}
// 	}
// }

// func (b *GPUBuilder) createDramControllerBuilder() dram.Builder {
// 	memBankSize := 4 * mem.GB / uint64(b.numMemoryBank)
// 	if 4*mem.GB%uint64(b.numMemoryBank) != 0 {
// 		panic("GPU memory size is not a multiple of the number of memory banks")
// 	}

// 	dramCol := 64
// 	dramRow := 16384
// 	dramDeviceWidth := 128
// 	dramBankSize := dramCol * dramRow * dramDeviceWidth
// 	dramBank := 4
// 	dramBankGroup := 4
// 	dramBusWidth := 256
// 	dramDevicePerRank := dramBusWidth / dramDeviceWidth
// 	dramRankSize := dramBankSize * dramDevicePerRank * dramBank
// 	dramRank := int(memBankSize * 8 / uint64(dramRankSize))

// 	memCtrlBuilder := dram.MakeBuilder().
// 		WithEngine(b.engine).
// 		WithFreq(500 * sim.MHz).
// 		WithProtocol(dram.HBM).
// 		WithBurstLength(4).
// 		WithDeviceWidth(dramDeviceWidth).
// 		WithBusWidth(dramBusWidth).
// 		WithNumChannel(1).
// 		WithNumRank(dramRank).
// 		WithNumBankGroup(dramBankGroup).
// 		WithNumBank(dramBank).
// 		WithNumCol(dramCol).
// 		WithNumRow(dramRow).
// 		WithCommandQueueSize(8).
// 		WithTransactionQueueSize(32).
// 		WithTCL(7).
// 		WithTCWL(2).
// 		WithTRCDRD(7).
// 		WithTRCDWR(7).
// 		WithTRP(7).
// 		WithTRAS(17).
// 		WithTREFI(1950).
// 		WithTRRDS(2).
// 		WithTRRDL(3).
// 		WithTWTRS(3).
// 		WithTWTRL(4).
// 		WithTWR(8).
// 		WithTCCDS(1).
// 		WithTCCDL(1).
// 		WithTRTRS(0).
// 		WithTRTP(3).
// 		WithTPPD(2)

// 	if b.visTracer != nil {
// 		memCtrlBuilder = memCtrlBuilder.WithAdditionalTracer(b.visTracer)
// 	}

// 	return memCtrlBuilder
// }

func (b *GPUBuilder) buildSA(
	saBuilder cdnaShaderArrayBuilder,
	saName string,
) {
	sa := saBuilder.Build(saName)

	b.populateCUs(&sa)
	b.populateROBs(&sa)
	b.populateTLBs(&sa)
	b.populateL1VAddressTranslators(&sa)
	b.populateL1Vs(&sa)
	b.populateScalerMemoryHierarchy(&sa)
	b.populateInstMemoryHierarchy(&sa)
}

func (b *GPUBuilder) populateCUs(sa *cdnaShaderArray) {
	for _, computeUnit := range sa.cus {
		b.cus = append(b.cus, computeUnit)
		b.gpu.CUs = append(b.gpu.CUs, computeUnit)

		if b.monitor != nil {
			b.monitor.RegisterComponent(computeUnit)
		}

		if b.enableISADebugging {
			isaDebug, err := os.Create(
				fmt.Sprintf("isa_timing_%s.debug", computeUnit.Name()))
			if err != nil {
				log.Fatal(err.Error())
			}
			isaDebugger := cu.NewISADebugger(
				log.New(isaDebug, "", 0),
				b.isa.CreateInstPrinter(nil),
			)
			tracing.CollectTrace(computeUnit, isaDebugger)
		}
	}
}

func (b *GPUBuilder) populateROBs(sa *cdnaShaderArray) {
	for _, rob := range sa.l1vROBs {
		b.l1vReorderBuffers = append(b.l1vReorderBuffers, rob)

		if b.monitor != nil {
			b.monitor.RegisterComponent(rob)
		}
	}
}

func (b *GPUBuilder) populateTLBs(sa *cdnaShaderArray) {
	for _, tlb := range sa.l1vTLBs {
		b.l1vTLBs = append(b.l1vTLBs, tlb)
		b.gpu.TLBs = append(b.gpu.TLBs, tlb)

		if b.monitor != nil {
			b.monitor.RegisterComponent(tlb)
		}
	}
}

func (b *GPUBuilder) populateL1Vs(sa *cdnaShaderArray) {
	for _, l1v := range sa.l1vCaches {
		b.l1vCaches = append(b.l1vCaches, l1v)
		b.gpu.Caches = append(b.gpu.Caches, l1v)

		if b.monitor != nil {
			b.monitor.RegisterComponent(l1v)
		}
	}
}

func (b *GPUBuilder) populateL1VAddressTranslators(sa *cdnaShaderArray) {
	for _, at := range sa.l1vATs {
		b.l1vAddrTrans = append(b.l1vAddrTrans, at)

		if b.monitor != nil {
			b.monitor.RegisterComponent(at)
		}
	}
}

func (b *GPUBuilder) populateScalerMemoryHierarchy(sa *cdnaShaderArray) {
	b.l1sAddrTrans = append(b.l1sAddrTrans, sa.l1sAT)
	b.l1sReorderBuffers = append(b.l1sReorderBuffers, sa.l1sROB)
	b.l1sCaches = append(b.l1sCaches, sa.l1sCache)
	b.gpu.Caches = append(b.gpu.Caches, sa.l1sCache)
	b.l1sTLBs = append(b.l1sTLBs, sa.l1sTLB)
	b.gpu.TLBs = append(b.gpu.TLBs, sa.l1sTLB)
}

func (b *GPUBuilder) populateInstMemoryHierarchy(sa *cdnaShaderArray) {
	b.l1iAddrTrans = append(b.l1iAddrTrans, sa.l1iAT)
	b.l1iReorderBuffers = append(b.l1iReorderBuffers, sa.l1iROB)
	b.l1iCaches = append(b.l1iCaches, sa.l1iCache)
	b.gpu.Caches = append(b.gpu.Caches, sa.l1iCache)
	b.l1iTLBs = append(b.l1iTLBs, sa.l1iTLB)
	b.gpu.TLBs = append(b.gpu.TLBs, sa.l1iTLB)
}

func (b *GPUBuilder) buildRDMAEngine() {
	b.rdmaEngine = rdma.NewEngine(
		fmt.Sprintf("%s.RDMA", b.gpuName),
		b.engine,
		b.lowModuleFinderForL1,
		nil,
	)
	b.gpu.RDMAEngine = b.rdmaEngine

	if b.monitor != nil {
		b.monitor.RegisterComponent(b.rdmaEngine)
	}
}

func (b *GPUBuilder) buildPageMigrationController() {
	b.pageMigrationController =
		pagemigrationcontroller.NewPageMigrationController(
			fmt.Sprintf("%s.PMC", b.gpuName),
			b.engine,
			b.lowModuleFinderForPMC,
			nil)
	b.gpu.PMC = b.pageMigrationController

	if b.monitor != nil {
		b.monitor.RegisterComponent(b.pageMigrationController)
	}
}

func (b *GPUBuilder) buildDMAEngine() {
	b.dmaEngine = cp.NewDMAEngine(
		fmt.Sprintf("%s.DMA", b.gpuName),
		b.engine,
		nil)

	if b.enableVisTracing {
		tracing.CollectTrace(b.dmaEngine, b.visTracer)
	}

	if b.monitor != nil {
		b.monitor.RegisterComponent(b.dmaEngine)
	}
}

func (b *GPUBuilder) buildCP() {
	builder := cp.MakeBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithMonitor(b.monitor).WithLatencyTable(b.latencyTable).
		WithConstantKernelOverhead(b.constantKernelOverhead)

	if b.enableVisTracing {
		builder = builder.WithVisTracer(b.visTracer)
	}

	b.cp = builder.Build(b.gpuName + ".CommandProcessor")
	b.gpu.CommandProcessor = b.cp

	if b.monitor != nil {
		b.monitor.RegisterComponent(b.cp)
	}

	b.buildDMAEngine()
	b.buildRDMAEngine()
	b.buildPageMigrationController()
}

func (b *GPUBuilder) buildL2TLB() {
	numWays := 128
	builder := tlb.MakeBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithNumWays(numWays).
		WithNumSets(int(b.dramSize / (1 << b.log2PageSize) / uint64(numWays))).
		WithNumMSHREntry(64).
		WithNumReqPerCycle(1024).
		WithPageSize(1 << b.log2PageSize).
		WithLowModule(b.mmu.GetPortByName("Top"))

	l2TLB := builder.Build(fmt.Sprintf("%s.L2TLB", b.gpuName))
	b.l2TLBs = append(b.l2TLBs, l2TLB)
	b.gpu.TLBs = append(b.gpu.TLBs, l2TLB)

	if b.enableVisTracing {
		tracing.CollectTrace(l2TLB, b.visTracer)
	}

	if b.monitor != nil {
		b.monitor.RegisterComponent(l2TLB)
	}
}

func (b *GPUBuilder) numCU() int {
	return b.numCUPerShaderArray * b.numShaderArray
}

func (b *GPUBuilder) connectWithDirectConnection(
	port1, port2 sim.Port,
	bufferSize int,
) {
	conn := sim.NewDirectConnection(
		port1.Name()+"-"+port2.Name(),
		b.engine, b.freq,
	)
	conn.PlugIn(port1, bufferSize)
	conn.PlugIn(port2, bufferSize)
}
