package gcn3

import (
	"fmt"

	wriatearound "gitlab.com/akita/mem/v2/cache/writearound"
	writethrough "gitlab.com/akita/mem/v2/cache/writethrough"
	"gitlab.com/akita/mgpusim/v2/timing/dcu"
	rob "gitlab.com/akita/mgpusim/v2/timing/rob"

	"gitlab.com/akita/akita/v2/sim"
	"gitlab.com/akita/mem/v2/mem"
	"gitlab.com/akita/mem/v2/vm/addresstranslator"
	"gitlab.com/akita/mem/v2/vm/tlb"
	"gitlab.com/akita/mgpusim/v2/isa"
	"gitlab.com/akita/util/v2/tracing"
)

type gcn3ShaderArray struct {
	cus []*dcu.ComputeUnit

	l1vROBs []*rob.ReorderBuffer
	l1sROBs []*rob.ReorderBuffer
	l1iROBs []*rob.ReorderBuffer

	l1vATs []*addresstranslator.AddressTranslator
	l1sATs []*addresstranslator.AddressTranslator
	l1iATs []*addresstranslator.AddressTranslator

	l1vCaches []*wriatearound.Cache
	l1sCaches []*writethrough.Cache
	l1iCaches []*writethrough.Cache

	l1vTLBs []*tlb.TLB
	l1sTLBs []*tlb.TLB
	l1iTLBs []*tlb.TLB
}

type gcn3ShaderArrayBuilder struct {
	gpuID uint64
	name  string
	numCU int

	engine            sim.Engine
	freq              sim.Freq
	isa               isa.ISA
	log2CacheLineSize uint64
	log2PageSize      uint64
	visTracer         tracing.Tracer
	memTracer         tracing.Tracer
}

func makeGCN3ShaderArrayBuilder() gcn3ShaderArrayBuilder {
	b := gcn3ShaderArrayBuilder{
		gpuID:             0,
		name:              "SA",
		numCU:             4,
		freq:              1 * sim.GHz,
		log2CacheLineSize: 6,
		log2PageSize:      12,
	}
	return b
}

func (b gcn3ShaderArrayBuilder) withEngine(e sim.Engine) gcn3ShaderArrayBuilder {
	b.engine = e
	return b
}

func (b gcn3ShaderArrayBuilder) withFreq(f sim.Freq) gcn3ShaderArrayBuilder {
	b.freq = f
	return b
}

func (b gcn3ShaderArrayBuilder) withISA(isa isa.ISA) gcn3ShaderArrayBuilder {
	b.isa = isa
	return b
}

func (b gcn3ShaderArrayBuilder) withGPUID(id uint64) gcn3ShaderArrayBuilder {
	b.gpuID = id
	return b
}

func (b gcn3ShaderArrayBuilder) withNumCU(n int) gcn3ShaderArrayBuilder {
	b.numCU = n
	return b
}

func (b gcn3ShaderArrayBuilder) withLog2CachelineSize(
	log2Size uint64,
) gcn3ShaderArrayBuilder {
	b.log2CacheLineSize = log2Size
	return b
}

func (b gcn3ShaderArrayBuilder) withLog2PageSize(
	log2Size uint64,
) gcn3ShaderArrayBuilder {
	b.log2PageSize = log2Size
	return b
}

func (b gcn3ShaderArrayBuilder) withVisTracer(
	visTracer tracing.Tracer,
) gcn3ShaderArrayBuilder {
	b.visTracer = visTracer
	return b
}

func (b gcn3ShaderArrayBuilder) withMemTracer(
	memTracer tracing.Tracer,
) gcn3ShaderArrayBuilder {
	b.memTracer = memTracer
	return b
}

func (b gcn3ShaderArrayBuilder) Build(name string) gcn3ShaderArray {
	b.name = name
	sa := gcn3ShaderArray{}

	b.buildComponents(&sa)
	b.connectComponents(&sa)

	return sa
}

func (b *gcn3ShaderArrayBuilder) buildComponents(sa *gcn3ShaderArray) {
	b.buildCUs(sa)

	b.buildL1VTLBs(sa)
	b.buildL1VAddressTranslators(sa)
	b.buildL1VReorderBuffers(sa)
	b.buildL1VCaches(sa)

	b.buildL1STLB(sa)
	b.buildL1SAddressTranslator(sa)
	b.buildL1SReorderBuffer(sa)
	b.buildL1SCache(sa)

	b.buildL1ITLB(sa)
	b.buildL1IAddressTranslator(sa)
	b.buildL1IReorderBuffer(sa)
	b.buildL1ICache(sa)
}

func (b *gcn3ShaderArrayBuilder) connectComponents(sa *gcn3ShaderArray) {
	b.connectVectorMem(sa)
	b.connectScalarMem(sa)
	b.connectInstMem(sa)
}

func (b *gcn3ShaderArrayBuilder) connectVectorMem(sa *gcn3ShaderArray) {
	for i := 0; i < b.numCU; i++ {
		cu := sa.cus[i]
		rob0 := sa.l1vROBs[i*2]
		rob1 := sa.l1vROBs[i*2+1]
		at0 := sa.l1vATs[i*2]
		at1 := sa.l1vATs[i*2+1]
		l0v0 := sa.l1vCaches[i*2]
		l0v1 := sa.l1vCaches[i*2+1]
		tlb0 := sa.l1vTLBs[i*2]
		tlb1 := sa.l1vTLBs[i*2+1]

		cu.VectorMemModules[0] = &mem.SingleLowModuleFinder{
			LowModule: rob0.GetPortByName("Top"),
		}
		cu.VectorMemModules[1] = &mem.SingleLowModuleFinder{
			LowModule: rob1.GetPortByName("Top"),
		}
		b.connect3WithDirectConnection(cu.ToVectorMem, rob0.GetPortByName("Top"), rob1.GetPortByName("Top"), 8)

		atTopPort0 := at0.GetPortByName("Top")
		rob0.BottomUnit = atTopPort0
		b.connectWithDirectConnection(
			rob0.GetPortByName("Bottom"), atTopPort0, 8)
		atTopPort1 := at1.GetPortByName("Top")
		rob1.BottomUnit = atTopPort1
		b.connectWithDirectConnection(
			rob1.GetPortByName("Bottom"), atTopPort1, 8)

		tlbTopPort0 := tlb0.GetPortByName("Top")
		at0.SetTranslationProvider(tlbTopPort0)
		b.connectWithDirectConnection(
			at0.GetPortByName("Translation"), tlbTopPort0, 8)
		tlbTopPort1 := tlb1.GetPortByName("Top")
		at1.SetTranslationProvider(tlbTopPort1)
		b.connectWithDirectConnection(
			at1.GetPortByName("Translation"), tlbTopPort1, 8)

		at0.SetLowModuleFinder(&mem.SingleLowModuleFinder{
			LowModule: l0v0.GetPortByName("Top"),
		})
		b.connectWithDirectConnection(l0v0.GetPortByName("Top"),
			at0.GetPortByName("Bottom"), 8)
		at1.SetLowModuleFinder(&mem.SingleLowModuleFinder{
			LowModule: l0v1.GetPortByName("Top"),
		})
		b.connectWithDirectConnection(l0v1.GetPortByName("Top"),
			at1.GetPortByName("Bottom"), 8)
	}
}

func (b *gcn3ShaderArrayBuilder) connectScalarMem(sa *gcn3ShaderArray) {
	for i := 0; i < b.numCU; i++ {
		cu := sa.cus[i]
		rob := sa.l1sROBs[i]
		at := sa.l1sATs[i]
		tlb := sa.l1sTLBs[i]
		l1s := sa.l1sCaches[i]

		cu.ScalarMem = rob.GetPortByName("Top")
		b.connectWithDirectConnection(cu.ToScalarMem,
			rob.GetPortByName("Top"), 8)

		atTopPort := at.GetPortByName("Top")
		rob.BottomUnit = atTopPort
		b.connectWithDirectConnection(rob.GetPortByName("Bottom"), atTopPort, 8)

		tlbTopPort := tlb.GetPortByName("Top")
		at.SetTranslationProvider(tlbTopPort)
		b.connectWithDirectConnection(
			at.GetPortByName("Translation"), tlbTopPort, 8)

		at.SetLowModuleFinder(&mem.SingleLowModuleFinder{
			LowModule: l1s.GetPortByName("Top"),
		})
		b.connectWithDirectConnection(
			l1s.GetPortByName("Top"), at.GetPortByName("Bottom"), 8)
	}
}

func (b *gcn3ShaderArrayBuilder) connectInstMem(sa *gcn3ShaderArray) {
	for i := 0; i < b.numCU; i++ {
		cu := sa.cus[i]
		rob := sa.l1iROBs[i]
		at := sa.l1iATs[i]
		tlb := sa.l1iTLBs[i]
		l1i := sa.l1iCaches[i]

		cu.InstMem = rob.GetPortByName("Top")
		b.connectWithDirectConnection(cu.ToInstMem,
			rob.GetPortByName("Top"), 8)
		l1iTopPort := l1i.GetPortByName("Top")
		rob.BottomUnit = l1iTopPort
		b.connectWithDirectConnection(rob.GetPortByName("Bottom"), l1iTopPort, 8)

		atTopPort := at.GetPortByName("Top")
		l1i.SetLowModuleFinder(&mem.SingleLowModuleFinder{
			LowModule: atTopPort,
		})
		b.connectWithDirectConnection(l1i.GetPortByName("Bottom"), atTopPort, 8)

		tlbTopPort := tlb.GetPortByName("Top")
		at.SetTranslationProvider(tlbTopPort)
		b.connectWithDirectConnection(
			at.GetPortByName("Translation"), tlbTopPort, 8)
	}
}

func (b *gcn3ShaderArrayBuilder) connectWithDirectConnection(
	port1, port2 sim.Port,
	bufferSize int,
) {
	name := fmt.Sprintf("%s-%s", port1.Name(), port2.Name())
	conn := sim.NewDirectConnection(
		name,
		b.engine, b.freq,
	)
	conn.PlugIn(port1, bufferSize)
	conn.PlugIn(port2, bufferSize)
}

func (b *gcn3ShaderArrayBuilder) connect3WithDirectConnection(
	port1, port2, port3 sim.Port,
	bufferSize int,
) {
	name := fmt.Sprintf("%s-%s-%s", port1.Name(), port2.Name(), port3.Name())
	conn := sim.NewDirectConnection(
		name,
		b.engine, b.freq,
	)
	conn.PlugIn(port1, bufferSize)
	conn.PlugIn(port2, bufferSize)
	conn.PlugIn(port3, bufferSize)
}
func (b *gcn3ShaderArrayBuilder) buildCUs(sa *gcn3ShaderArray) {
	cuBuilder := dcu.MakeBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithLog2CachelineSize(b.log2CacheLineSize).
		WithISA(b.isa).
		WithVectorMemPipelineLatency(110)

	for i := 0; i < b.numCU; i++ {
		cuName := fmt.Sprintf("%s.CU_%02d", b.name, i)
		cu := cuBuilder.Build(cuName)
		sa.cus = append(sa.cus, cu)

		if b.visTracer != nil {
			tracing.CollectTrace(cu, b.visTracer)
		}
	}
}

func (b *gcn3ShaderArrayBuilder) buildL1VReorderBuffers(sa *gcn3ShaderArray) {
	builder := rob.MakeBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithBufferSize(128).
		WithNumReqPerCycle(4)

	for i := 0; i < b.numCU; i++ {
		name := fmt.Sprintf("%s.L1VROB_%02d_00", b.name, i)
		rob0 := builder.Build(name)
		sa.l1vROBs = append(sa.l1vROBs, rob0)

		if b.visTracer != nil {
			tracing.CollectTrace(rob0, b.visTracer)
		}
		name = fmt.Sprintf("%s.L1VROB_%02d_01", b.name, i)
		rob1 := builder.Build(name)
		sa.l1vROBs = append(sa.l1vROBs, rob1)

		if b.visTracer != nil {
			tracing.CollectTrace(rob1, b.visTracer)
		}
	}
}

func (b *gcn3ShaderArrayBuilder) buildL1VAddressTranslators(sa *gcn3ShaderArray) {
	builder := addresstranslator.MakeBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithDeviceID(b.gpuID).
		WithLog2PageSize(b.log2PageSize)

	for i := 0; i < b.numCU; i++ {
		name := fmt.Sprintf("%s.L1VAddrTrans_%02d_00", b.name, i)
		at0 := builder.Build(name)
		sa.l1vATs = append(sa.l1vATs, at0)

		if b.visTracer != nil {
			tracing.CollectTrace(at0, b.visTracer)
		}
		name = fmt.Sprintf("%s.L1VAddrTrans_%02d_01", b.name, i)
		at1 := builder.Build(name)
		sa.l1vATs = append(sa.l1vATs, at1)

		if b.visTracer != nil {
			tracing.CollectTrace(at1, b.visTracer)
		}
	}
}

func (b *gcn3ShaderArrayBuilder) buildL1VTLBs(sa *gcn3ShaderArray) {
	builder := tlb.MakeBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithNumMSHREntry(4).
		WithNumSets(1).
		WithNumWays(64).
		WithNumReqPerCycle(4)

	for i := 0; i < b.numCU; i++ {
		name := fmt.Sprintf("%s.L1VTLB_%02d_00", b.name, i)
		tlb0 := builder.Build(name)
		sa.l1vTLBs = append(sa.l1vTLBs, tlb0)

		if b.visTracer != nil {
			tracing.CollectTrace(tlb0, b.visTracer)
		}
		name = fmt.Sprintf("%s.L1VTLB_%02d_01", b.name, i)
		tlb1 := builder.Build(name)
		sa.l1vTLBs = append(sa.l1vTLBs, tlb1)

		if b.visTracer != nil {
			tracing.CollectTrace(tlb1, b.visTracer)
		}
	}
}

func (b *gcn3ShaderArrayBuilder) buildL1VCaches(sa *gcn3ShaderArray) {
	builder := wriatearound.NewBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithNumBanks(1).
		WithLog2BlockSize(b.log2CacheLineSize).
		WithWayAssociativity(4).
		WithNumMSHREntry(16).
		WithTotalByteSize(16 * mem.KB).
		WithDirectoryLatency(2).
		WithBankLatency(5)

	if b.visTracer != nil {
		builder = builder.WithVisTracer(b.visTracer)
	}

	for i := 0; i < b.numCU; i++ {
		name := fmt.Sprintf("%s.L1VCache_%02d_00", b.name, i)
		cache0 := builder.Build(name)
		sa.l1vCaches = append(sa.l1vCaches, cache0)

		if b.memTracer != nil {
			tracing.CollectTrace(cache0, b.memTracer)
		}
		name = fmt.Sprintf("%s.L1VCache_%02d_01", b.name, i)
		cache1 := builder.Build(name)
		sa.l1vCaches = append(sa.l1vCaches, cache1)

		if b.memTracer != nil {
			tracing.CollectTrace(cache1, b.memTracer)
		}
	}
}

func (b *gcn3ShaderArrayBuilder) buildL1SReorderBuffer(sa *gcn3ShaderArray) {
	builder := rob.MakeBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithBufferSize(128).
		WithNumReqPerCycle(4)
	for i := 0; i < b.numCU; i++ {
		name := fmt.Sprintf("%s.L1SROB_%02d", b.name, i)
		rob := builder.Build(name)
		sa.l1sROBs = append(sa.l1sROBs, rob)

		if b.visTracer != nil {
			tracing.CollectTrace(rob, b.visTracer)
		}
	}
}

func (b *gcn3ShaderArrayBuilder) buildL1SAddressTranslator(sa *gcn3ShaderArray) {
	builder := addresstranslator.MakeBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithDeviceID(b.gpuID).
		WithLog2PageSize(b.log2PageSize)
	for i := 0; i < b.numCU; i++ {
		name := fmt.Sprintf("%s.L1SAddrTrans_%02d", b.name, i)
		at := builder.Build(name)
		sa.l1sATs = append(sa.l1sATs, at)

		if b.visTracer != nil {
			tracing.CollectTrace(at, b.visTracer)
		}
	}
}

func (b *gcn3ShaderArrayBuilder) buildL1STLB(sa *gcn3ShaderArray) {
	builder := tlb.MakeBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithNumMSHREntry(4).
		WithNumSets(1).
		WithNumWays(64).
		WithNumReqPerCycle(4)
	for i := 0; i < b.numCU; i++ {
		name := fmt.Sprintf("%s.L1STLB_%02d", b.name, i)
		tlb := builder.Build(name)
		sa.l1sTLBs = append(sa.l1sTLBs, tlb)

		if b.visTracer != nil {
			tracing.CollectTrace(tlb, b.visTracer)
		}
	}
}

func (b *gcn3ShaderArrayBuilder) buildL1SCache(sa *gcn3ShaderArray) {
	builder := writethrough.NewBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithBankLatency(1).
		WithNumBanks(1).
		WithLog2BlockSize(b.log2CacheLineSize).
		WithWayAssociativity(4).
		WithNumMSHREntry(16).
		WithTotalByteSize(16 * mem.KB)
	for i := 0; i < b.numCU; i++ {
		name := fmt.Sprintf("%s.L1SCache_%02d", b.name, i)
		cache := builder.Build(name)
		sa.l1sCaches = append(sa.l1sCaches, cache)

		if b.visTracer != nil {
			tracing.CollectTrace(cache, b.visTracer)
		}
		if b.memTracer != nil {
			tracing.CollectTrace(cache, b.memTracer)
		}
	}
}

func (b *gcn3ShaderArrayBuilder) buildL1IReorderBuffer(sa *gcn3ShaderArray) {
	builder := rob.MakeBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithBufferSize(128).
		WithNumReqPerCycle(4)
	for i := 0; i < b.numCU; i++ {
		name := fmt.Sprintf("%s.L1IROB_%02d", b.name, i)
		rob := builder.Build(name)
		sa.l1iROBs = append(sa.l1iROBs, rob)

		if b.visTracer != nil {
			tracing.CollectTrace(rob, b.visTracer)
		}
	}
}

func (b *gcn3ShaderArrayBuilder) buildL1IAddressTranslator(sa *gcn3ShaderArray) {
	builder := addresstranslator.MakeBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithDeviceID(b.gpuID).
		WithLog2PageSize(b.log2PageSize)
	for i := 0; i < b.numCU; i++ {
		name := fmt.Sprintf("%s.L1IAddrTrans_%02d", b.name, i)
		at := builder.Build(name)
		sa.l1iATs = append(sa.l1iATs, at)

		if b.visTracer != nil {
			tracing.CollectTrace(at, b.visTracer)
		}
	}
}

func (b *gcn3ShaderArrayBuilder) buildL1ITLB(sa *gcn3ShaderArray) {
	builder := tlb.MakeBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithNumMSHREntry(4).
		WithNumSets(1).
		WithNumWays(64).
		WithNumReqPerCycle(4)
	for i := 0; i < b.numCU; i++ {
		name := fmt.Sprintf("%s.L1ITLB_%02d", b.name, i)
		tlb := builder.Build(name)
		sa.l1iTLBs = append(sa.l1iTLBs, tlb)

		if b.visTracer != nil {
			tracing.CollectTrace(tlb, b.visTracer)
		}
	}
}

func (b *gcn3ShaderArrayBuilder) buildL1ICache(sa *gcn3ShaderArray) {
	builder := writethrough.NewBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithBankLatency(1).
		WithNumBanks(1).
		WithLog2BlockSize(b.log2CacheLineSize).
		WithWayAssociativity(4).
		WithNumMSHREntry(16).
		WithTotalByteSize(32 * mem.KB).
		WithNumReqsPerCycle(4)
	for i := 0; i < b.numCU; i++ {
		name := fmt.Sprintf("%s.L1ICache_%02d", b.name, i)
		cache := builder.Build(name)
		sa.l1iCaches = append(sa.l1iCaches, cache)

		if b.visTracer != nil {
			tracing.CollectTrace(cache, b.visTracer)
		}

		if b.memTracer != nil {
			tracing.CollectTrace(cache, b.memTracer)
		}
	}
}
