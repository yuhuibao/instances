package runner

import (
	"fmt"

	"gitlab.com/akita/akita/v2/sim"
	"gitlab.com/akita/mem/v2/mem"
	"gitlab.com/akita/mem/v2/vm/mmu"
	"gitlab.com/akita/mgpusim/v2/driver"
	"gitlab.com/yuhuibao/instances/P2DCUL1/runner/navi"
	"gitlab.com/yuhuibao/instances/P2DCUL1/runner/platform"
)

func (b *PlatformBuilder) createRX5500XTGPUs() {
	gpuBuilder := b.createRX5500XTGPUBuilder(b.engine, b.gpuDriver, b.mmuComponent)
	lastSwitchID := b.rootComplexID
	for i := 1; i < b.numGPU+1; i++ {
		if i%2 == 1 {
			lastSwitchID = b.pcieConnector.AddSwitch(b.rootComplexID)
		}

		b.createRX5500XTGPU(i, gpuBuilder, lastSwitchID)
	}
}

func (b *PlatformBuilder) createRX5500XTGPUBuilder(
	engine sim.Engine,
	gpuDriver *driver.Driver,
	mmuComponent *mmu.MMU,
) navi.GPUBuilder {
	latencyTable := []int{
		1,
		10, 6, 10, 13,
		16, 20, 24, 27,
		31, 35, 39, 43,
		47, 51, 55, 59,
		63, 63, 63, 63,
		63, 63, 63, 63,
		63, 63, 63, 63,
		63, 63, 63, 63,
	}
	constantKernelOverhead := 300
	gpuBuilder := navi.MakeGPUBuilder().
		WithEngine(engine).
		WithFreq(1845 * sim.MHz).
		WithISA(b.isa).
		WithMMU(mmuComponent).
		WithNumCUPerShaderArray([]int{4, 4, 3}).
		WithNumMemoryBank(8).
		WithL2CacheSize(1 * mem.MB).
		WithDRAMSize(4 * mem.GB).
		WithLog2CacheLineSize(7).
		WithLog2MemoryBankInterleavingSize(8).
		WithLog2PageSize(b.log2PageSize).
		WithLatencyTable(latencyTable).
		WithConstantKernelOverhead(constantKernelOverhead)

	if b.monitor != nil {
		gpuBuilder = gpuBuilder.WithMonitor(b.monitor)
	}

	if b.debugISA {
		gpuBuilder = gpuBuilder.WithISADebugging()
	}

	if b.visTracer != nil {
		gpuBuilder = gpuBuilder.WithVisTracer(b.visTracer)
	}

	if b.memTracer != nil {
		gpuBuilder = gpuBuilder.WithMemTracer(b.memTracer)
	}

	return gpuBuilder
}

func (b *PlatformBuilder) createRX5500XTGPU(
	index int,
	gpuBuilder navi.GPUBuilder,
	pcieSwitchID int,
) *platform.GPU {
	name := fmt.Sprintf("GPU%d", index)
	memAddrOffset := uint64(index) * 4 * mem.GB
	gpu := gpuBuilder.
		WithMemAddrOffset(memAddrOffset).
		Build(name, uint64(index))
	b.gpuDriver.RegisterGPU(
		gpu.Domain.GetPortByName("CommandProcessor"),
		b.isa.Name(),
		4*mem.GB)
	gpu.CommandProcessor.Driver = b.gpuDriver.GetPortByName("GPU")

	b.configRDMAEngine(gpu, b.rdmaAddrTable)
	b.configPMC(gpu, b.gpuDriver, b.pmcAddrTable)

	b.pcieConnector.PlugInDevice(pcieSwitchID, gpu.Domain.Ports())

	b.gpus = append(b.gpus, gpu)

	return gpu
}
