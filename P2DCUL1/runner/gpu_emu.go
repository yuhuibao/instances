package runner

import (
	"fmt"

	"gitlab.com/akita/mem/v2/mem"
	"gitlab.com/yuhuibao/instances/P2DCUL1/runner/emu"
	"gitlab.com/yuhuibao/instances/P2DCUL1/runner/platform"
)

func (b *PlatformBuilder) createEMUGPUs() {
	storage := mem.NewStorage(uint64(b.numGPU+1) * 4 * mem.GB)
	gpuBuilder := b.createEMUGPUBuilder(storage)
	lastSwitchID := b.rootComplexID
	for i := 1; i < b.numGPU+1; i++ {
		if i%2 == 1 {
			lastSwitchID = b.pcieConnector.AddSwitch(b.rootComplexID)
		}

		b.createEMUGPU(i, gpuBuilder, lastSwitchID)
	}
}

func (b *PlatformBuilder) createEMUGPUBuilder(
	storage *mem.Storage,
) emu.GPUBuilder {
	gpuBuilder := emu.MakeGPUBuilder().
		WithEngine(b.engine).
		WithDriver(b.gpuDriver).
		WithPageTable(b.pageTable).
		WithLog2PageSize(b.log2PageSize).
		WithMemCapacity(4 * mem.GB).
		WithStorage(storage)

	if b.debugISA {
		gpuBuilder = gpuBuilder.WithISADebugging()
	}

	if b.traceMem {
		gpuBuilder = gpuBuilder.WithMemTracing()
	}

	return gpuBuilder
}

func (b *PlatformBuilder) createEMUGPU(
	index int,
	gpuBuilder emu.GPUBuilder,
	pcieSwitchID int,
) *platform.GPU {
	name := fmt.Sprintf("GPU%d", index)
	memAddrOffset := uint64(index) * 4 * mem.GB
	gpu := gpuBuilder.
		WithISA(b.isa.Name()).
		WithMemOffset(memAddrOffset).
		Build(name)
	b.gpuDriver.RegisterGPU(
		gpu.Domain.GetPortByName("CommandProcessor"),
		b.isa.Name(),
		4*mem.GB)
	gpu.CommandProcessor.Driver = b.gpuDriver.GetPortByName("GPU")

	b.pcieConnector.PlugInDevice(pcieSwitchID, gpu.Domain.Ports())

	b.gpus = append(b.gpus, gpu)

	return gpu
}
