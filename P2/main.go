package main

import (
	"flag"

	_ "net/http/pprof"

	"gitlab.com/yuhuibao/instances/P2/runner"
	"gitlab.com/yuhuibao/instances/benchmarkselection"
)

var benchmarkFlag = flag.String("benchmark", "fir", "Which benchmark to run")

func main() {
	flag.Parse()

	runner := new(runner.Runner).ParseFlag().Init()

	benchmark := benchmarkselection.SelectBenchmark(*benchmarkFlag, runner.Driver())
	runner.AddBenchmark(benchmark)
	runner.Run()
}
