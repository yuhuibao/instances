package runner

import (
	"log"
	"os"

	memtraces "gitlab.com/akita/mem/v2/trace"

	"gitlab.com/akita/akita/v2/monitoring"
	"gitlab.com/akita/akita/v2/sim"
	"gitlab.com/akita/mem/v2/mem"
	"gitlab.com/akita/mem/v2/vm"
	"gitlab.com/akita/mem/v2/vm/mmu"
	"gitlab.com/akita/mgpusim/v2/driver"
	"gitlab.com/akita/mgpusim/v2/isa"
	"gitlab.com/akita/mgpusim/v2/isa/gcn3"
	"gitlab.com/akita/noc/v2/networking/pcie"
	"gitlab.com/akita/util/v2/tracing"
	"gitlab.com/yuhuibao/instances/P3CU/runner/platform"
)

// PlatformBuilder can build a full system can include a given number of GPUs.
type PlatformBuilder struct {
	useParallelEngine bool
	debugISA          bool
	traceVis          bool
	visTraceStartTime sim.VTimeInSec
	visTraceEndTime   sim.VTimeInSec
	traceMem          bool
	numGPU            int
	log2PageSize      uint64
	monitor           *monitoring.Monitor
	isa               isa.ISA
	gpu               string

	engine                      sim.Engine
	gpus                        []*platform.GPU
	mmuComponent                *mmu.MMU
	pageTable                   vm.PageTable
	gpuDriver                   *driver.Driver
	pcieConnector               *pcie.Connector
	rootComplexID               int
	rdmaAddrTable, pmcAddrTable *mem.BankedLowModuleFinder
	visTracer                   tracing.Tracer
	memTracer                   tracing.Tracer
}

// MakePlatformBuild creates a EmuBuilder with default parameters.
func MakePlatformBuild() PlatformBuilder {
	b := PlatformBuilder{
		numGPU:            4,
		gpu:               "R9Nano",
		isa:               gcn3.ISA{},
		log2PageSize:      12,
		visTraceStartTime: -1,
		visTraceEndTime:   -1,
	}
	return b
}

// WithParallelEngine lets the EmuBuilder to use parallel engine.
func (b PlatformBuilder) WithParallelEngine() PlatformBuilder {
	b.useParallelEngine = true
	return b
}

// WithISADebugging enables ISA debugging in the simulation.
func (b PlatformBuilder) WithISADebugging() PlatformBuilder {
	b.debugISA = true
	return b
}

// WithVisTracing lets the platform to record traces for visualization purposes.
func (b PlatformBuilder) WithVisTracing() PlatformBuilder {
	b.traceVis = true
	return b
}

// WithPartialVisTracing lets the platform to record traces for visualization
// purposes. The trace will only be collected from the start time to the end
// time.
func (b PlatformBuilder) WithPartialVisTracing(
	start, end sim.VTimeInSec,
) PlatformBuilder {
	b.traceVis = true
	b.visTraceStartTime = start
	b.visTraceEndTime = end

	return b
}

// WithMemTracing lets the platform to trace memory operations.
func (b PlatformBuilder) WithMemTracing() PlatformBuilder {
	b.traceMem = true
	return b
}

// WithNumGPU sets the number of GPUs to build.
func (b PlatformBuilder) WithNumGPU(n int) PlatformBuilder {
	b.numGPU = n
	return b
}

// WithLog2PageSize sets the page size as a power of 2.
func (b PlatformBuilder) WithLog2PageSize(
	n uint64,
) PlatformBuilder {
	b.log2PageSize = n
	return b
}

// WithMonitor sets the monitor that is used to monitor the simulation
func (b PlatformBuilder) WithMonitor(
	m *monitoring.Monitor,
) PlatformBuilder {
	b.monitor = m
	return b
}

// WithISA sets the ISA that the GPUs use.
func (b PlatformBuilder) WithISA(
	isa isa.ISA,
) PlatformBuilder {
	b.isa = isa
	return b
}

// WithGPU sets the type of GPU to use in the system.
func (b PlatformBuilder) WithGPU(gpu string) PlatformBuilder {
	b.gpu = gpu
	return b
}

// Build builds a platform with GPUs.
func (b PlatformBuilder) Build() *platform.Platform {
	b.createTracers()
	b.createEngine()
	b.createMMU()
	b.createDriver()
	b.createConnection()

	b.mmuComponent.MigrationServiceProvider = b.gpuDriver.GetPortByName("MMU")

	b.rdmaAddrTable = b.createRDMAAddrTable()
	b.pmcAddrTable = b.createPMCPageTable()

	b.createGPUs()

	b.pcieConnector.EstablishRoute()

	return &platform.Platform{
		Engine: b.engine,
		Driver: b.gpuDriver,
		GPUs:   b.gpus,
	}
}

func (b *PlatformBuilder) createTracers() {
	b.createMemTracer()
	b.createVisTracer()
}

func (b *PlatformBuilder) createMemTracer() {
	if !b.traceMem {
		return
	}

	file, err := os.Create("mem.trace")
	if err != nil {
		panic(err)
	}
	logger := log.New(file, "", 0)
	b.memTracer = memtraces.NewTracer(logger)
}

func (b *PlatformBuilder) createVisTracer() {
	if !b.traceVis {
		return
	}

	tracer := tracing.NewMySQLTracerWithTimeRange(
		b.visTraceStartTime,
		b.visTraceEndTime)
	tracer.Init()

	b.visTracer = tracer
}

func (b *PlatformBuilder) createDriver() {
	b.gpuDriver = driver.NewDriver(b.engine, b.pageTable, b.log2PageSize)
	// file, err := os.Create("driver_comm.csv")
	// if err != nil {
	// 	panic(err)
	// }
	// gpuDriver.GetPortByName("GPU").AcceptHook(
	// 	sim.NewPortMsgLogger(log.New(file, "", 0)))

	if b.monitor != nil {
		b.monitor.RegisterComponent(b.gpuDriver)
	}
}

func (b *PlatformBuilder) createGPUs() {
	switch b.gpu {
	case "R9Nano":
		b.createR9NanoGPUs()
	case "RX5500XT":
		b.createRX5500XTGPUs()
	case "MI100":
		b.createMI100GPUs()
	case "EMUGPU":
		b.createEMUGPUs()
	default:
		panic("GPU " + b.gpu + " is not supported.")
	}
}

func (b PlatformBuilder) createPMCPageTable() *mem.BankedLowModuleFinder {
	pmcAddressTable := new(mem.BankedLowModuleFinder)
	pmcAddressTable.BankSize = 4 * mem.GB
	pmcAddressTable.LowModules = append(pmcAddressTable.LowModules, nil)
	return pmcAddressTable
}

func (b PlatformBuilder) createRDMAAddrTable() *mem.BankedLowModuleFinder {
	rdmaAddressTable := new(mem.BankedLowModuleFinder)
	rdmaAddressTable.BankSize = 4 * mem.GB
	rdmaAddressTable.LowModules = append(rdmaAddressTable.LowModules, nil)
	return rdmaAddressTable
}

func (b *PlatformBuilder) createConnection() {
	// connection := sim.NewDirectConnection(engine)
	// connection := noc.NewFixedBandwidthConnection(32, engine, 1*sim.GHz)
	// connection.SrcBufferCapacity = 40960000
	b.pcieConnector = pcie.NewConnector().
		WithEngine(b.engine).
		WithFrequency(1*sim.GHz).
		WithVersion(3, 16).
		WithSwitchLatency(20)
	b.pcieConnector.CreateNetwork("PCIe")
	b.rootComplexID = b.pcieConnector.AddRootComplex(
		[]sim.Port{
			b.gpuDriver.GetPortByName("GPU"),
			b.gpuDriver.GetPortByName("MMU"),
			b.mmuComponent.GetPortByName("Migration"),
			b.mmuComponent.GetPortByName("Top"),
		})
}

func (b *PlatformBuilder) createEngine() {
	if b.useParallelEngine {
		b.engine = sim.NewParallelEngine()
	} else {
		b.engine = sim.NewSerialEngine()
	}
	// engine.AcceptHook(sim.NewEventLogger(log.New(os.Stdout, "", 0)))

	if b.monitor != nil {
		b.monitor.RegisterEngine(b.engine)
	}
}

func (b *PlatformBuilder) createMMU() {
	b.pageTable = vm.NewPageTable(b.log2PageSize)
	mmuBuilder := mmu.MakeBuilder().
		WithEngine(b.engine).
		WithFreq(1 * sim.GHz).
		WithPageWalkingLatency(5).
		WithLog2PageSize(b.log2PageSize).
		WithPageTable(b.pageTable)

	b.mmuComponent = mmuBuilder.Build("MMU")

	if b.monitor != nil {
		b.monitor.RegisterComponent(b.mmuComponent)
	}
}

func (b *PlatformBuilder) configRDMAEngine(
	gpu *platform.GPU,
	addrTable *mem.BankedLowModuleFinder,
) {
	gpu.RDMAEngine.RemoteRDMAAddressTable = addrTable
	addrTable.LowModules = append(
		addrTable.LowModules,
		gpu.RDMAEngine.ToOutside)
}

func (b *PlatformBuilder) configPMC(
	gpu *platform.GPU,
	gpuDriver *driver.Driver,
	addrTable *mem.BankedLowModuleFinder,
) {
	gpu.PMC.RemotePMCAddressTable = addrTable
	addrTable.LowModules = append(
		addrTable.LowModules,
		gpu.PMC.GetPortByName("Remote"))
	gpuDriver.RemotePMCPorts = append(
		gpuDriver.RemotePMCPorts, gpu.PMC.GetPortByName("Remote"))
}
