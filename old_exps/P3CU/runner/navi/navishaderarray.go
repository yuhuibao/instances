package navi

import (
	"fmt"

	"gitlab.com/akita/akita/v2/sim"
	"gitlab.com/akita/mem/v2/cache/writearound"
	"gitlab.com/akita/mem/v2/cache/writeevict"
	"gitlab.com/akita/mem/v2/cache/writethrough"
	"gitlab.com/akita/mem/v2/mem"
	"gitlab.com/akita/mem/v2/vm"

	"gitlab.com/akita/mem/v2/vm/addresstranslator"
	// "gitlab.com/akita/mem/v2/vm/idealaddresstranslator"
	"gitlab.com/akita/mem/v2/vm/tlb"
	"gitlab.com/akita/mgpusim/v2/isa"
	"gitlab.com/akita/mgpusim/v2/timing/cu"
	"gitlab.com/akita/mgpusim/v2/timing/rob"
	"gitlab.com/akita/util/v2/tracing"
)

type naviShaderArray struct {
	cus []*cu.ComputeUnit

	l0vROBs []*rob.ReorderBuffer
	l0sROBs []*rob.ReorderBuffer
	l0iROBs []*rob.ReorderBuffer

	l0vATs []*addresstranslator.AddressTranslator
	// l0vATs []*idealaddresstranslator.AddressTranslator
	l0sATs []*addresstranslator.AddressTranslator
	l0iATs []*addresstranslator.AddressTranslator

	l0vCaches []*writearound.Cache
	l0sCaches []*writethrough.Cache
	l0iCaches []*writethrough.Cache

	l0vTLBs []*tlb.TLB
	l0sTLBs []*tlb.TLB
	l0iTLBs []*tlb.TLB

	l1Cache *writeevict.Cache
	l1Conn  *sim.DirectConnection
}

type naviShaderArrayBuilder struct {
	gpuID  uint64
	name   string
	numDCU int

	engine            sim.Engine
	freq              sim.Freq
	isa               isa.ISA
	log2CacheLineSize uint64
	log2PageSize      uint64
	visTracer         tracing.Tracer
	memTracer         tracing.Tracer
	pageTable         vm.PageTable
}

func makeNaviShaderArrayBuilder() naviShaderArrayBuilder {
	b := naviShaderArrayBuilder{
		gpuID:             0,
		name:              "SA",
		numDCU:            4,
		freq:              1 * sim.GHz,
		log2CacheLineSize: 7,
		log2PageSize:      12,
	}
	return b
}

func (b naviShaderArrayBuilder) withEngine(e sim.Engine) naviShaderArrayBuilder {
	b.engine = e
	return b
}

func (b naviShaderArrayBuilder) withFreq(f sim.Freq) naviShaderArrayBuilder {
	b.freq = f
	return b
}

func (b naviShaderArrayBuilder) withISA(isa isa.ISA) naviShaderArrayBuilder {
	b.isa = isa
	return b
}

func (b naviShaderArrayBuilder) withGPUID(id uint64) naviShaderArrayBuilder {
	b.gpuID = id
	return b
}

func (b naviShaderArrayBuilder) withNumDCU(n int) naviShaderArrayBuilder {
	b.numDCU = n
	return b
}

func (b naviShaderArrayBuilder) withLog2CachelineSize(
	log2Size uint64,
) naviShaderArrayBuilder {
	b.log2CacheLineSize = log2Size
	return b
}

func (b naviShaderArrayBuilder) withLog2PageSize(
	log2Size uint64,
) naviShaderArrayBuilder {
	b.log2PageSize = log2Size
	return b
}

func (b naviShaderArrayBuilder) withVisTracer(
	visTracer tracing.Tracer,
) naviShaderArrayBuilder {
	b.visTracer = visTracer
	return b
}

func (b naviShaderArrayBuilder) withMemTracer(
	memTracer tracing.Tracer,
) naviShaderArrayBuilder {
	b.memTracer = memTracer
	return b
}

func (b naviShaderArrayBuilder) WithPageTable(pageTable vm.PageTable) naviShaderArrayBuilder {
	b.pageTable = pageTable
	return b
}

func (b naviShaderArrayBuilder) Build(name string) naviShaderArray {
	b.name = name
	sa := naviShaderArray{}

	b.buildComponents(&sa)
	b.connectComponents(&sa)

	return sa
}

func (b *naviShaderArrayBuilder) buildComponents(sa *naviShaderArray) {
	b.buildDCUs(sa)

	b.buildL1Cache(sa)

	b.buildL0VTLBs(sa)
	b.buildL0VAddressTranslators(sa)
	b.buildL0VReorderBuffers(sa)
	b.buildL0VCaches(sa)

	b.buildL0STLB(sa)
	b.buildL0SAddressTranslator(sa)
	b.buildL0SReorderBuffer(sa)
	b.buildL0SCache(sa)

	b.buildL0ITLB(sa)
	b.buildL0IAddressTranslator(sa)
	b.buildL0IReorderBuffer(sa)
	b.buildL0ICache(sa)
}

func (b *naviShaderArrayBuilder) connectComponents(sa *naviShaderArray) {
	b.connectL1Cache(sa)
	b.connectVectorMem(sa)
	b.connectScalarMem(sa)
	b.connectInstMem(sa)
}

func (b *naviShaderArrayBuilder) connectVectorMem(sa *naviShaderArray) {
	for i := 0; i < b.numDCU; i++ {
		cu := sa.cus[i]
		rob := sa.l0vROBs[i]
		at := sa.l0vATs[i]
		l0v := sa.l0vCaches[i]
		tlb := sa.l0vTLBs[i]

		cu.VectorMemModules = &mem.SingleLowModuleFinder{
			LowModule: rob.GetPortByName("Top"),
		}
		b.connectWithDirectConnection(cu.ToVectorMem,
			rob.GetPortByName("Top"), 8)

		atTopPort := at.GetPortByName("Top")
		rob.BottomUnit = atTopPort
		b.connectWithDirectConnection(
			rob.GetPortByName("Bottom"), atTopPort, 8)

		tlbTopPort := tlb.GetPortByName("Top")
		at.SetTranslationProvider(tlbTopPort)
		b.connectWithDirectConnection(
			at.GetPortByName("Translation"), tlbTopPort, 8)

		at.SetLowModuleFinder(&mem.SingleLowModuleFinder{
			LowModule: l0v.GetPortByName("Top"),
		})
		b.connectWithDirectConnection(l0v.GetPortByName("Top"),
			at.GetPortByName("Bottom"), 8)
	}
}

func (b *naviShaderArrayBuilder) connectScalarMem(sa *naviShaderArray) {
	for i := 0; i < b.numDCU; i++ {
		rob := sa.l0sROBs[i]
		at := sa.l0sATs[i]
		tlb := sa.l0sTLBs[i]
		l0s := sa.l0sCaches[i]

		atTopPort := at.GetPortByName("Top")
		rob.BottomUnit = atTopPort
		b.connectWithDirectConnection(rob.GetPortByName("Bottom"), atTopPort, 8)

		tlbTopPort := tlb.GetPortByName("Top")
		at.SetTranslationProvider(tlbTopPort)
		b.connectWithDirectConnection(
			at.GetPortByName("Translation"), tlbTopPort, 8)

		at.SetLowModuleFinder(&mem.SingleLowModuleFinder{
			LowModule: l0s.GetPortByName("Top"),
		})
		b.connectWithDirectConnection(
			l0s.GetPortByName("Top"), at.GetPortByName("Bottom"), 8)

		robTopPort := rob.GetPortByName("Top")
		dcu := sa.cus[i]
		dcu.ScalarMem = robTopPort
		b.connectWithDirectConnection(
			robTopPort,
			dcu.ToScalarMem,
			8,
		)
	}
}

func (b *naviShaderArrayBuilder) connectInstMem(sa *naviShaderArray) {
	for i := 0; i < b.numDCU; i++ {
		rob := sa.l0iROBs[i]
		at := sa.l0iATs[i]
		tlb := sa.l0iTLBs[i]
		l0i := sa.l0iCaches[i]

		l0iTopPort := l0i.GetPortByName("Top")
		rob.BottomUnit = l0iTopPort
		b.connectWithDirectConnection(rob.GetPortByName("Bottom"), l0iTopPort, 8)

		atTopPort := at.GetPortByName("Top")
		l0i.SetLowModuleFinder(&mem.SingleLowModuleFinder{
			LowModule: atTopPort,
		})
		b.connectWithDirectConnection(l0i.GetPortByName("Bottom"), atTopPort, 8)

		tlbTopPort := tlb.GetPortByName("Top")
		at.SetTranslationProvider(tlbTopPort)
		b.connectWithDirectConnection(
			at.GetPortByName("Translation"), tlbTopPort, 8)

		robTopPort := rob.GetPortByName("Top")
		dcu := sa.cus[i]
		dcu.InstMem = robTopPort
		b.connectWithDirectConnection(
			robTopPort, dcu.ToInstMem, 8)
	}
}

func (b *naviShaderArrayBuilder) connectL1Cache(sa *naviShaderArray) {
	l1TopPort := sa.l1Cache.GetPortByName("Top")
	l1ConnName := fmt.Sprintf("%s.L1CacheConn", b.name)
	l1Conn := sim.NewDirectConnection(l1ConnName, b.engine, b.freq)
	sa.l1Conn = l1Conn
	l1Conn.PlugIn(l1TopPort, 8)

	for _, at := range sa.l0iATs {
		at.SetLowModuleFinder(&mem.SingleLowModuleFinder{
			LowModule: l1TopPort,
		})
		l1Conn.PlugIn(at.GetPortByName("Bottom"), 8)
	}

	for _, cache := range sa.l0sCaches {
		cache.SetLowModuleFinder(&mem.SingleLowModuleFinder{
			LowModule: l1TopPort,
		})
		l1Conn.PlugIn(cache.GetPortByName("Bottom"), 8)
	}

	for _, cache := range sa.l0vCaches {
		cache.SetLowModuleFinder(&mem.SingleLowModuleFinder{
			LowModule: l1TopPort,
		})
		l1Conn.PlugIn(cache.GetPortByName("Bottom"), 8)
	}
}

func (b *naviShaderArrayBuilder) connectWithDirectConnection(
	port1, port2 sim.Port,
	bufferSize int,
) {
	name := fmt.Sprintf("%s-%s", port1.Name(), port2.Name())
	conn := sim.NewDirectConnection(
		name,
		b.engine, b.freq,
	)
	conn.PlugIn(port1, bufferSize)
	conn.PlugIn(port2, bufferSize)
}

func (b *naviShaderArrayBuilder) buildDCUs(sa *naviShaderArray) {
	dcuBuilder := cu.MakeBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithLog2CachelineSize(7). // Every transaction can only be 128B in size.
		WithVectorMemPipelineLatency(32).
		WithISA(b.isa)

	for i := 0; i < b.numDCU; i++ {
		dcuName := fmt.Sprintf("%s.DCU_%02d.DCU", b.name, i)
		dcu := dcuBuilder.Build(dcuName)
		sa.cus = append(sa.cus, dcu)

		if b.visTracer != nil {
			tracing.CollectTrace(dcu, b.visTracer)
		}
	}
}

func (b *naviShaderArrayBuilder) buildL0VReorderBuffers(sa *naviShaderArray) {
	builder := rob.MakeBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithBufferSize(64).
		WithNumReqPerCycle(1024)

	for i := 0; i < b.numDCU; i++ {
		name := fmt.Sprintf("%s.DCU_%02d.L0VROB_%02d", b.name, i, 0)
		rob := builder.Build(name)
		sa.l0vROBs = append(sa.l0vROBs, rob)

		if b.visTracer != nil {
			tracing.CollectTrace(rob, b.visTracer)
		}
	}
}

func (b *naviShaderArrayBuilder) buildL0VAddressTranslators(sa *naviShaderArray) {
	builder := addresstranslator.MakeBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithDeviceID(b.gpuID).
		WithNumReqPerCycle(1024).
		WithLog2PageSize(b.log2PageSize)
	// builder := idealaddresstranslator.MakeBuilder().
	// 	WithEngine(b.engine).
	// 	WithFreq(b.freq).
	// 	WithDeviceID(b.gpuID).
	// 	WithNumReqPerCycle(1024).
	// 	WithLog2PageSize(b.log2PageSize).
	// 	WithPageTable(b.pageTable)

	for i := 0; i < b.numDCU; i++ {
		name := fmt.Sprintf("%s.DCU_%02d.L0VAddrTrans_%02d", b.name, i, 0)
		at := builder.Build(name)
		sa.l0vATs = append(sa.l0vATs, at)

		if b.visTracer != nil {
			tracing.CollectTrace(at, b.visTracer)
		}
	}
}

func (b *naviShaderArrayBuilder) buildL0VTLBs(sa *naviShaderArray) {
	builder := tlb.MakeBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithNumMSHREntry(40).
		WithNumSets(1).
		WithNumWays(64).
		WithNumReqPerCycle(4)

	for i := 0; i < b.numDCU; i++ {
		name := fmt.Sprintf("%s.DCU_%02d.L0VTLB_%02d", b.name, i, 0)
		tlb := builder.Build(name)
		sa.l0vTLBs = append(sa.l0vTLBs, tlb)

		if b.visTracer != nil {
			tracing.CollectTrace(tlb, b.visTracer)
		}
	}
}

func (b *naviShaderArrayBuilder) buildL0VCaches(sa *naviShaderArray) {
	builder := writearound.NewBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithBankLatency(36).
		WithNumBanks(1).
		WithLog2BlockSize(b.log2CacheLineSize).
		WithWayAssociativity(4).
		WithNumMSHREntry(1024).
		WithNumReqsPerCycle(4).
		WithTotalByteSize(16 * mem.KB).
		WithMaxNumConcurrentTrans(1024)

	if b.visTracer != nil {
		builder = builder.WithVisTracer(b.visTracer)
	}

	for i := 0; i < b.numDCU; i++ {
		name := fmt.Sprintf("%s.DCU_%02d.L0VCache_%02d", b.name, i, 0)
		cache := builder.Build(name)

		sa.l0vCaches = append(sa.l0vCaches, cache)

		if b.memTracer != nil {
			tracing.CollectTrace(cache, b.memTracer)
		}
	}
}

func (b *naviShaderArrayBuilder) buildL0SReorderBuffer(sa *naviShaderArray) {
	builder := rob.MakeBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithBufferSize(32).
		WithNumReqPerCycle(4)

	for i := 0; i < b.numDCU; i++ {
		name := fmt.Sprintf("%s.DCU_%02d.L0SROB", b.name, i)
		rob := builder.Build(name)
		sa.l0sROBs = append(sa.l0sROBs, rob)

		if b.visTracer != nil {
			tracing.CollectTrace(rob, b.visTracer)
		}
	}
}

func (b *naviShaderArrayBuilder) buildL0SAddressTranslator(sa *naviShaderArray) {
	builder := addresstranslator.MakeBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithDeviceID(b.gpuID).
		WithLog2PageSize(b.log2PageSize).
		WithNumReqPerCycle(4)

	for i := 0; i < b.numDCU; i++ {
		name := fmt.Sprintf("%s.DCU_%02d.L0SAddrTrans", b.name, i)
		at := builder.Build(name)
		sa.l0sATs = append(sa.l0sATs, at)

		if b.visTracer != nil {
			tracing.CollectTrace(at, b.visTracer)
		}
	}
}

func (b *naviShaderArrayBuilder) buildL0STLB(sa *naviShaderArray) {
	builder := tlb.MakeBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithNumMSHREntry(4).
		WithNumSets(1).
		WithNumWays(64).
		WithNumReqPerCycle(4)

	for i := 0; i < b.numDCU; i++ {
		name := fmt.Sprintf("%s.DCU_%02d.L0STLB", b.name, i)
		tlb := builder.Build(name)
		sa.l0sTLBs = append(sa.l0sTLBs, tlb)

		if b.visTracer != nil {
			tracing.CollectTrace(tlb, b.visTracer)
		}
	}
}

func (b *naviShaderArrayBuilder) buildL0SCache(sa *naviShaderArray) {
	builder := writethrough.NewBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithBankLatency(1).
		WithNumBanks(1).
		WithLog2BlockSize(b.log2CacheLineSize).
		WithWayAssociativity(4).
		WithNumMSHREntry(16).
		WithNumReqsPerCycle(4).
		WithTotalByteSize(16 * mem.KB)

	for i := 0; i < b.numDCU; i++ {
		name := fmt.Sprintf("%s.DCU_%02d.L0SCache", b.name, i)
		cache := builder.Build(name)

		sa.l0sCaches = append(sa.l0sCaches, cache)

		if b.visTracer != nil {
			tracing.CollectTrace(cache, b.visTracer)
		}

		if b.memTracer != nil {
			tracing.CollectTrace(cache, b.memTracer)
		}
	}
}

func (b *naviShaderArrayBuilder) buildL0IReorderBuffer(sa *naviShaderArray) {
	builder := rob.MakeBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithBufferSize(128).
		WithNumReqPerCycle(4)

	for i := 0; i < b.numDCU; i++ {
		name := fmt.Sprintf("%s.DCU_%02d.L0IROB", b.name, i)
		rob := builder.Build(name)
		sa.l0iROBs = append(sa.l0iROBs, rob)

		if b.visTracer != nil {
			tracing.CollectTrace(rob, b.visTracer)
		}
	}
}

func (b *naviShaderArrayBuilder) buildL0IAddressTranslator(sa *naviShaderArray) {
	builder := addresstranslator.MakeBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithDeviceID(b.gpuID).
		WithNumReqPerCycle(4).
		WithLog2PageSize(b.log2PageSize)

	for i := 0; i < b.numDCU; i++ {
		name := fmt.Sprintf("%s.DCU_%02d.L0IAddrTrans", b.name, i)
		at := builder.Build(name)
		sa.l0iATs = append(sa.l0iATs, at)

		if b.visTracer != nil {
			tracing.CollectTrace(at, b.visTracer)
		}
	}
}

func (b *naviShaderArrayBuilder) buildL0ITLB(sa *naviShaderArray) {
	builder := tlb.MakeBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithNumMSHREntry(4).
		WithNumSets(1).
		WithNumWays(64).
		WithNumReqPerCycle(4)

	for i := 0; i < b.numDCU; i++ {
		name := fmt.Sprintf("%s.DCU_%02d.L0ITLB", b.name, i)
		tlb := builder.Build(name)
		sa.l0iTLBs = append(sa.l0iTLBs, tlb)

		if b.visTracer != nil {
			tracing.CollectTrace(tlb, b.visTracer)
		}
	}
}

func (b *naviShaderArrayBuilder) buildL0ICache(sa *naviShaderArray) {
	builder := writethrough.NewBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithBankLatency(1).
		WithNumBanks(1).
		WithLog2BlockSize(b.log2CacheLineSize).
		WithWayAssociativity(4).
		WithNumMSHREntry(8).
		WithTotalByteSize(32 * mem.KB).
		WithNumReqsPerCycle(4).
		WithMaxNumConcurrentTrans(32)

	for i := 0; i < b.numDCU; i++ {
		name := fmt.Sprintf("%s.DCU_%02d.L0ICache", b.name, i)
		cache := builder.Build(name)
		sa.l0iCaches = append(sa.l0iCaches, cache)

		if b.visTracer != nil {
			tracing.CollectTrace(cache, b.visTracer)
		}

		if b.memTracer != nil {
			tracing.CollectTrace(cache, b.memTracer)
		}
	}
}

func (b *naviShaderArrayBuilder) buildL1Cache(sa *naviShaderArray) {
	builder := writeevict.NewBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithDirectoryLatency(12).
		WithBankLatency(45).
		WithNumBanks(1).
		WithLog2BlockSize(b.log2CacheLineSize).
		WithWayAssociativity(16).
		WithNumMSHREntry(1024).
		WithTotalByteSize(128 * mem.KB).
		WithNumReqsPerCycle(16).
		WithMaxNumConcurrentTrans(1024)

	name := fmt.Sprintf("%s.L1Cache", b.name)
	cache := builder.Build(name)
	sa.l1Cache = cache

	if b.visTracer != nil {
		tracing.CollectTrace(cache, b.visTracer)
	}

	if b.memTracer != nil {
		tracing.CollectTrace(cache, b.memTracer)
	}
}
