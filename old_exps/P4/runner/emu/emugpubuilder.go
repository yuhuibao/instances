// Package emu defines how an emulation GPU is configured.
package emu

import (
	"fmt"
	"log"
	"os"

	"gitlab.com/akita/akita/v2/sim"
	"gitlab.com/akita/mem/v2/idealmemcontroller"
	"gitlab.com/akita/mem/v2/mem"
	memtraces "gitlab.com/akita/mem/v2/trace"
	"gitlab.com/akita/mem/v2/vm"
	"gitlab.com/akita/mgpusim/v2/driver"
	cdnaemu "gitlab.com/akita/mgpusim/v2/isa/cdna/emu"
	gcn3emu "gitlab.com/akita/mgpusim/v2/isa/gcn3/emu"
	naviemu "gitlab.com/akita/mgpusim/v2/isa/navi/emu"
	"gitlab.com/akita/mgpusim/v2/timing/cp"
	"gitlab.com/akita/util/v2/tracing"
	"gitlab.com/yuhuibao/instances/P4/runner/platform"
)

// GPUBuilder provide services to assemble usable GPUs
type GPUBuilder struct {
	engine           sim.Engine
	freq             sim.Freq
	driver           *driver.Driver
	pageTable        vm.PageTable
	log2PageSize     uint64
	memOffset        uint64
	memCapacity      uint64
	gpuName          string
	gpu              *sim.Domain
	storage          *mem.Storage
	commandProcessor *cp.CommandProcessor
	gpuMem           *idealmemcontroller.Comp
	dmaEngine        *cp.DMAEngine
	computeUnits     []sim.Component
	isa              string

	enableISADebug   bool
	enableMemTracing bool
}

// MakeGPUBuilder creates a new GPUBuilder.
func MakeGPUBuilder() GPUBuilder {
	b := GPUBuilder{}
	b.freq = 1 * sim.GHz
	b.log2PageSize = 12

	b.enableISADebug = false
	return b
}

// WithEngine sets the engine that the emulator GPUs to use
func (b GPUBuilder) WithEngine(e sim.Engine) GPUBuilder {
	b.engine = e
	return b
}

// WithDriver sets the GPU driver that the GPUs connect to.
func (b GPUBuilder) WithDriver(d *driver.Driver) GPUBuilder {
	b.driver = d
	return b
}

// WithISA sets the ISA that the emulation GPU uses.
func (b GPUBuilder) WithISA(isa string) GPUBuilder {
	b.isa = isa
	return b
}

// WithPageTable sets the page table that provides the address translation
func (b GPUBuilder) WithPageTable(pageTable vm.PageTable) GPUBuilder {
	b.pageTable = pageTable
	return b
}

// WithLog2PageSize sets the page size of the GPU, as a power of 2.
func (b GPUBuilder) WithLog2PageSize(n uint64) GPUBuilder {
	b.log2PageSize = n
	return b
}

// WithMemCapacity sets the capacity of the GPU memory
func (b GPUBuilder) WithMemCapacity(c uint64) GPUBuilder {
	b.memCapacity = c
	return b
}

// WithMemOffset sets the first byte address of the GPU memory
func (b GPUBuilder) WithMemOffset(offset uint64) GPUBuilder {
	b.memOffset = offset
	return b
}

// WithStorage sets the global memory storage that is shared by multiple GPUs
func (b GPUBuilder) WithStorage(s *mem.Storage) GPUBuilder {
	b.storage = s
	return b
}

// WithISADebugging enables the simulation to dump instruction execution
// information.
func (b GPUBuilder) WithISADebugging() GPUBuilder {
	b.enableISADebug = true
	return b
}

// WithMemTracing enables the simulation to dump memory transaction information.
func (b GPUBuilder) WithMemTracing() GPUBuilder {
	b.enableMemTracing = true
	return b
}

// Build creates a very simple GPU for emulation purposes
func (b GPUBuilder) Build(name string) *platform.GPU {
	b.clear()
	b.gpuName = name
	b.buildMemory()
	b.buildComputeUnits()
	b.buildGPU()
	b.connectInternalComponents()
	b.populateExternalPorts()

	return &platform.GPU{
		Domain:           b.gpu,
		CommandProcessor: b.commandProcessor,
	}
}

func (b *GPUBuilder) populateExternalPorts() {
	b.gpu.AddPort("CommandProcessor", b.commandProcessor.ToDriver)
}

func (b *GPUBuilder) clear() {
	b.commandProcessor = nil
	b.computeUnits = nil
	b.gpuMem = nil
	b.dmaEngine = nil
	b.gpu = nil
}

func (b *GPUBuilder) buildComputeUnits() {
	for i := 0; i < 64; i++ {
		name := fmt.Sprintf("%s.CU_%d", b.gpuName, i)
		var computeUnit sim.Component
		switch b.isa {
		case "gfx803":
			computeUnit = b.buildGCN3EmuCU(name)
		case "gfx906", "gfx908":
			computeUnit = b.buildCDNAEmuCU(name)
		case "gfx1010":
			computeUnit = b.buildNaviEmuCU(name)
		default:
			panic("isa " + b.isa + " not supported")
		}

		b.computeUnits = append(b.computeUnits, computeUnit)
	}
}

func (b *GPUBuilder) buildGCN3EmuCU(
	name string,
) sim.Component {
	computeUnit := gcn3emu.MakeCUBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithStorage(b.gpuMem.Storage).
		WithPageTable(b.pageTable).
		WithLog2PageSize(b.log2PageSize).
		Build(name)

	if b.enableISADebug {
		isaDebug, err := os.Create(
			fmt.Sprintf("isa_%s.debug", computeUnit.Name()))
		if err != nil {
			log.Fatal(err.Error())
		}
		isaDebugger := gcn3emu.NewISADebugger(log.New(isaDebug, "", 0))
		computeUnit.AcceptHook(isaDebugger)
	}
	return computeUnit
}

func (b *GPUBuilder) buildCDNAEmuCU(
	name string,
) sim.Component {
	computeUnit := cdnaemu.MakeCUBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithStorage(b.gpuMem.Storage).
		WithPageTable(b.pageTable).
		WithLog2PageSize(b.log2PageSize).
		Build(name)

	if b.enableISADebug {
		isaDebug, err := os.Create(
			fmt.Sprintf("isa_%s.debug", computeUnit.Name()))
		if err != nil {
			log.Fatal(err.Error())
		}
		isaDebugger := cdnaemu.NewISADebugger(log.New(isaDebug, "", 0))
		computeUnit.AcceptHook(isaDebugger)
	}
	return computeUnit
}

func (b *GPUBuilder) buildNaviEmuCU(
	name string,
) sim.Component {
	computeUnit := naviemu.MakeCUBuilder().
		WithEngine(b.engine).
		WithFreq(b.freq).
		WithStorage(b.gpuMem.Storage).
		WithPageTable(b.pageTable).
		WithLog2PageSize(b.log2PageSize).
		Build(name)

	if b.enableISADebug {
		isaDebug, err := os.Create(
			fmt.Sprintf("isa_%s.debug", computeUnit.Name()))
		if err != nil {
			log.Fatal(err.Error())
		}
		isaDebugger := naviemu.NewISADebugger(log.New(isaDebug, "", 0))
		computeUnit.AcceptHook(isaDebugger)
	}
	return computeUnit
}

func (b *GPUBuilder) buildMemory() {
	b.gpuMem = idealmemcontroller.New(
		b.gpuName+".GlobalMem", b.engine, b.memCapacity)
	b.gpuMem.Freq = 1 * sim.GHz
	b.gpuMem.Latency = 1
	b.gpuMem.Storage = b.storage

	if b.enableMemTracing {
		file, _ := os.Create("mem.trace")
		logger := log.New(file, "", 0)
		memTracer := memtraces.NewTracer(logger)
		tracing.CollectTrace(b.gpuMem, memTracer)
	}
}

func (b *GPUBuilder) buildGPU() {
	b.commandProcessor = cp.MakeBuilder().
		WithEngine(b.engine).
		WithFreq(1 * sim.GHz).
		WithLatencyTable([]int{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}).
		Build(b.gpuName + ".CommandProcessor")

	b.gpu = sim.NewDomain(b.gpuName)
	b.commandProcessor.Driver = b.driver.GetPortByName("GPU")

	localDataSource := new(mem.SingleLowModuleFinder)
	localDataSource.LowModule = b.gpuMem.GetPortByName("Top")
	b.dmaEngine = cp.NewDMAEngine(
		fmt.Sprintf("%s.DMA", b.gpuName), b.engine, localDataSource)
	b.commandProcessor.DMAEngine = b.dmaEngine.ToCP
}

func (b *GPUBuilder) connectInternalComponents() {
	connection := sim.NewDirectConnection(
		"InterGPUConn", b.engine, 1*sim.GHz)

	connection.PlugIn(b.commandProcessor.ToDMA, 1)
	connection.PlugIn(b.commandProcessor.ToCUs, 1)
	connection.PlugIn(b.gpuMem.GetPortByName("Top"), 1)
	connection.PlugIn(b.dmaEngine.ToCP, 1)
	connection.PlugIn(b.dmaEngine.ToMem, 1)

	for _, cu := range b.computeUnits {
		b.commandProcessor.RegisterCU(cu.(cp.CUInterfaceForCP))
		connection.PlugIn(cu.GetPortByName("Dispatching"), 4)
	}
}
