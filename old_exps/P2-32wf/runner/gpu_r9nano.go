package runner

import (
	"fmt"

	"gitlab.com/akita/mem/v2/mem"
	"gitlab.com/yuhuibao/instances/P2-32wf/runner/gcn3"
	"gitlab.com/yuhuibao/instances/P2-32wf/runner/platform"
)

func (b *PlatformBuilder) createR9NanoGPUs() {
	gpuBuilder := b.createR9NanoGPUBuilder()
	lastSwitchID := b.rootComplexID
	for i := 1; i < b.numGPU+1; i++ {
		if i%2 == 1 {
			lastSwitchID = b.pcieConnector.AddSwitch(b.rootComplexID)
		}

		b.createR9NanoGPU(i, gpuBuilder, lastSwitchID)
	}
}

func (b *PlatformBuilder) createR9NanoGPUBuilder() gcn3.GPUBuilder {
	latencyTable := []int{
		1,
		4, 4, 4, 4,
		5, 6, 7, 8,
		9, 10, 11, 12,
		13, 14, 15, 16,
		16, 16, 16, 16,
		16, 16, 16, 16,
		16, 16, 16, 16,
		16, 16, 16, 16,
	}
	constantKernelOverhead := 1600
	gpuBuilder := gcn3.MakeGPUBuilder().
		WithEngine(b.engine).
		WithISA(b.isa).
		WithMMU(b.mmuComponent).
		WithNumCUPerShaderArray(4).
		WithNumShaderArray(45).
		WithNumMemoryBank(16).
		WithL2CacheSize(2 * mem.MB).
		WithDRAMSize(4 * mem.GB).
		WithLog2MemoryBankInterleavingSize(7).
		WithLog2PageSize(b.log2PageSize).
		WithLatencyTable(latencyTable).
		WithConstantKernelOverhead(constantKernelOverhead)

	if b.monitor != nil {
		gpuBuilder = gpuBuilder.WithMonitor(b.monitor)
	}

	if b.debugISA {
		gpuBuilder = gpuBuilder.WithISADebugging()
	}

	if b.visTracer != nil {
		gpuBuilder = gpuBuilder.WithVisTracer(b.visTracer)
	}

	if b.memTracer != nil {
		gpuBuilder = gpuBuilder.WithMemTracer(b.memTracer)
	}

	return gpuBuilder
}

func (b *PlatformBuilder) createR9NanoGPU(
	index int,
	gpuBuilder gcn3.GPUBuilder,
	pcieSwitchID int,
) *platform.GPU {
	name := fmt.Sprintf("GPU%d", index)
	memAddrOffset := uint64(index) * 4 * mem.GB
	gpu := gpuBuilder.
		WithMemAddrOffset(memAddrOffset).
		Build(name, uint64(index))

	b.gpuDriver.RegisterGPU(
		gpu.Domain.GetPortByName("CommandProcessor"),
		b.isa.Name(),
		4*mem.GB)
	gpu.CommandProcessor.Driver = b.gpuDriver.GetPortByName("GPU")

	b.configRDMAEngine(gpu, b.rdmaAddrTable)
	b.configPMC(gpu, b.gpuDriver, b.pmcAddrTable)

	b.pcieConnector.PlugInDevice(pcieSwitchID, gpu.Domain.Ports())

	b.gpus = append(b.gpus, gpu)

	return gpu
}
