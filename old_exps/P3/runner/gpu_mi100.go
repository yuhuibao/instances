package runner

import (
	"fmt"

	"gitlab.com/akita/mem/v2/mem"
	"gitlab.com/yuhuibao/instances/P3/runner/cdna"
	"gitlab.com/yuhuibao/instances/P3/runner/platform"
)

func (b *PlatformBuilder) createMI100GPUs() {
	gpuBuilder := b.createMI100GPUBuilder()
	lastSwitchID := b.rootComplexID
	for i := 1; i < b.numGPU+1; i++ {
		if i%2 == 1 {
			lastSwitchID = b.pcieConnector.AddSwitch(b.rootComplexID)
		}

		b.createMI100GPU(i, gpuBuilder, lastSwitchID)
	}
}

func (b *PlatformBuilder) createMI100GPUBuilder() cdna.GPUBuilder {
	gpuBuilder := cdna.MakeGPUBuilder().
		WithEngine(b.engine).
		WithISA(b.isa).
		WithMMU(b.mmuComponent).
		WithNumCUPerShaderArray(4).
		WithNumShaderArray(16).
		WithNumMemoryBank(16).
		WithL2CacheSize(8 * mem.MB).
		WithLog2MemoryBankInterleavingSize(7).
		WithLog2PageSize(b.log2PageSize)

	if b.monitor != nil {
		gpuBuilder = gpuBuilder.WithMonitor(b.monitor)
	}

	if b.debugISA {
		gpuBuilder = gpuBuilder.WithISADebugging()
	}

	if b.visTracer != nil {
		gpuBuilder = gpuBuilder.WithVisTracer(b.visTracer)
	}

	if b.memTracer != nil {
		gpuBuilder = gpuBuilder.WithMemTracer(b.memTracer)
	}

	return gpuBuilder
}

func (b *PlatformBuilder) createMI100GPU(
	index int,
	gpuBuilder cdna.GPUBuilder,
	pcieSwitchID int,
) *platform.GPU {
	name := fmt.Sprintf("GPU%d", index)
	memAddrOffset := uint64(index) * 4 * mem.GB
	gpu := gpuBuilder.
		WithMemAddrOffset(memAddrOffset).
		Build(name, uint64(index))

	b.gpuDriver.RegisterGPU(
		gpu.Domain.GetPortByName("CommandProcessor"),
		b.isa.Name(),
		4*mem.GB)
	gpu.CommandProcessor.Driver = b.gpuDriver.GetPortByName("GPU")

	b.configRDMAEngine(gpu, b.rdmaAddrTable)
	b.configPMC(gpu, b.gpuDriver, b.pmcAddrTable)

	b.pcieConnector.PlugInDevice(pcieSwitchID, gpu.Domain.Ports())

	b.gpus = append(b.gpus, gpu)

	return gpu
}
