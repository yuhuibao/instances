package benchmarkselection

import (
	"gitlab.com/akita/mgpusim/v2/benchmarks"
	"gitlab.com/akita/mgpusim/v2/benchmarks/amdappsdk/bitonicsort"
	"gitlab.com/akita/mgpusim/v2/benchmarks/amdappsdk/fastwalshtransform"
	"gitlab.com/akita/mgpusim/v2/benchmarks/amdappsdk/floydwarshall"
	"gitlab.com/akita/mgpusim/v2/benchmarks/amdappsdk/matrixtranspose"
	"gitlab.com/akita/mgpusim/v2/benchmarks/dnn/relu"
	"gitlab.com/akita/mgpusim/v2/benchmarks/heteromark/fir"
	"gitlab.com/akita/mgpusim/v2/benchmarks/heteromark/kmeans"
	"gitlab.com/akita/mgpusim/v2/benchmarks/heteromark/pagerank"
	"gitlab.com/akita/mgpusim/v2/benchmarks/polybench/atax"
	"gitlab.com/akita/mgpusim/v2/benchmarks/polybench/bicg"
	"gitlab.com/akita/mgpusim/v2/benchmarks/shoc/spmv"
	"gitlab.com/akita/mgpusim/v2/driver"
)

func SelectBenchmark(name string, driver *driver.Driver) benchmarks.Benchmark {
	var benchmark benchmarks.Benchmark
	switch name {
	case "atax":
		atax := atax.NewBenchmark(driver)
		atax.NX = 4096
		atax.NY = 4096
		benchmark = atax
	case "bicg":
		bicg := bicg.NewBenchmark(driver)
		bicg.NX = 4096
		bicg.NY = 4096
		benchmark = bicg
	case "bitonicsort":
		bitonicsort := bitonicsort.NewBenchmark(driver)
		bitonicsort.Length = 655360
		// bitonicsort.Length = 655360 hangs
		benchmark = bitonicsort
	case "fastwalshtransform":
		fastwalshtransform := fastwalshtransform.NewBenchmark(driver)
		fastwalshtransform.Length = 1048576
		benchmark = fastwalshtransform
	case "fir":
		fir := fir.NewBenchmark(driver)
		fir.Length = 10485760
		benchmark = fir
	case "floydwarshall":
		floydwarshall := floydwarshall.NewBenchmark(driver)
		floydwarshall.NumNodes = 512
		floydwarshall.NumIterations = 512
		benchmark = floydwarshall
	case "kmeans":
		kmeans := kmeans.NewBenchmark(driver)
		kmeans.NumPoints = 524288
		kmeans.NumClusters = 8
		kmeans.NumFeatures = 32
		kmeans.MaxIter = 3
		benchmark = kmeans
	case "matrixtranspose":
		matrixtranspose := matrixtranspose.NewBenchmark(driver)
		matrixtranspose.Width = 4096
		benchmark = matrixtranspose
	case "pagerank":
		pagerank := pagerank.NewBenchmark(driver)
		pagerank.NumNodes = 262144
		pagerank.NumConnections = 1048576
		pagerank.MaxIterations = 3
		benchmark = pagerank
	case "relu":
		relu := relu.NewBenchmark(driver)
		relu.Length = 10485760
		benchmark = relu
	case "spmv":
		spmv := spmv.NewBenchmark(driver)
		spmv.Dim = 10485760
		spmv.Sparsity = 0.000000001
		benchmark = spmv
	default:
		panic("Unknown benchmark")
	}

	return benchmark
}
