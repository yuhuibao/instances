import subprocess
import os
from multiprocessing.pool import ThreadPool
from datetime import datetime

exps = [
    ("P2DCU", "atax", ["-num-of-sa=8", "-isa=gfx1010"]),
    ("P2DCU", "bicg", ["-num-of-sa=8", "-isa=gfx1010"]),
    ("P2DCU", "bitonicsort", ["-num-of-sa=8", "-isa=gfx1010"]),
    ("P2DCU", "fir", ["-num-of-sa=8", "-isa=gfx1010"]),
    ("P2DCU", "floydwarshall", ["-num-of-sa=8", "-isa=gfx1010"]),
    ("P2DCU", "fastwalshtransform", ["-num-of-sa=8", "-isa=gfx1010"]),
    ("P2DCU", "kmeans", ["-num-of-sa=8", "-isa=gfx1010"]),
    ("P2DCU", "matrixtranspose", ["-num-of-sa=8", "-isa=gfx1010"]),
    ("P2DCU", "pagerank", ["-num-of-sa=8", "-isa=gfx1010"]),
    ("P2DCU", "relu", ["-num-of-sa=8", "-isa=gfx1010"]),
    ("P2DCU", "spmv", ["-num-of-sa=8", "-isa=gfx1010"]),
    ("P2DCU", "atax", ["-num-of-sa=16", "-isa=gfx1010"]),
    ("P2DCU", "bicg", ["-num-of-sa=16", "-isa=gfx1010"]),
    ("P2DCU", "bitonicsort", ["-num-of-sa=16", "-isa=gfx1010"]),
    ("P2DCU", "fir", ["-num-of-sa=16", "-isa=gfx1010"]),
    ("P2DCU", "floydwarshall", ["-num-of-sa=16", "-isa=gfx1010"]),
    ("P2DCU", "fastwalshtransform", ["-num-of-sa=16", "-isa=gfx1010"]),
    ("P2DCU", "kmeans", ["-num-of-sa=16", "-isa=gfx1010"]),
    ("P2DCU", "matrixtranspose", ["-num-of-sa=16", "-isa=gfx1010"]),
    ("P2DCU", "pagerank", ["-num-of-sa=16", "-isa=gfx1010"]),
    ("P2DCU", "relu", ["-num-of-sa=16", "-isa=gfx1010"]),
    ("P2DCU", "spmv", ["-num-of-sa=16", "-isa=gfx1010"]),
    ("P2DCU", "atax", ["-num-of-sa=32", "-isa=gfx1010"]),
    ("P2DCU", "bicg", ["-num-of-sa=32", "-isa=gfx1010"]),
    ("P2DCU", "bitonicsort", ["-num-of-sa=32", "-isa=gfx1010"]),
    ("P2DCU", "fir", ["-num-of-sa=32", "-isa=gfx1010"]),
    ("P2DCU", "floydwarshall", ["-num-of-sa=32", "-isa=gfx1010"]),
    ("P2DCU", "fastwalshtransform", ["-num-of-sa=32", "-isa=gfx1010"]),
    ("P2DCU", "kmeans", ["-num-of-sa=32", "-isa=gfx1010"]),
    ("P2DCU", "matrixtranspose", ["-num-of-sa=32", "-isa=gfx1010"]),
    ("P2DCU", "pagerank", ["-num-of-sa=32", "-isa=gfx1010"]),
    ("P2DCU", "relu", ["-num-of-sa=32", "-isa=gfx1010"]),
    ("P2DCU", "spmv", ["-num-of-sa=32", "-isa=gfx1010"]),
    ("P2DCU", "atax", ["-num-of-sa=64", "-isa=gfx1010"]),
    ("P2DCU", "bicg", ["-num-of-sa=64", "-isa=gfx1010"]),
    ("P2DCU", "bitonicsort", ["-num-of-sa=64", "-isa=gfx1010"]),
    ("P2DCU", "fir", ["-num-of-sa=64", "-isa=gfx1010"]),
    ("P2DCU", "floydwarshall", ["-num-of-sa=64", "-isa=gfx1010"]),
    ("P2DCU", "fastwalshtransform", ["-num-of-sa=64", "-isa=gfx1010"]),
    ("P2DCU", "kmeans", ["-num-of-sa=64", "-isa=gfx1010"]),
    ("P2DCU", "matrixtranspose", ["-num-of-sa=64", "-isa=gfx1010"]),
    ("P2DCU", "pagerank", ["-num-of-sa=64", "-isa=gfx1010"]),
    ("P2DCU", "relu", ["-num-of-sa=64", "-isa=gfx1010"]),
    ("P2DCU", "spmv", ["-num-of-sa=64", "-isa=gfx1010"]),
    ("P2DCUL1", "atax", ["-num-of-sa=8", "-isa=gfx1010"]),
    ("P2DCUL1", "bicg", ["-num-of-sa=8", "-isa=gfx1010"]),
    ("P2DCUL1", "bitonicsort", ["-num-of-sa=8", "-isa=gfx1010"]),
    ("P2DCUL1", "fir", ["-num-of-sa=8", "-isa=gfx1010"]),
    ("P2DCUL1", "floydwarshall", ["-num-of-sa=8", "-isa=gfx1010"]),
    ("P2DCUL1", "fastwalshtransform", ["-num-of-sa=8", "-isa=gfx1010"]),
    ("P2DCUL1", "kmeans", ["-num-of-sa=8", "-isa=gfx1010"]),
    ("P2DCUL1", "matrixtranspose", ["-num-of-sa=8", "-isa=gfx1010"]),
    ("P2DCUL1", "pagerank", ["-num-of-sa=8", "-isa=gfx1010"]),
    ("P2DCUL1", "relu", ["-num-of-sa=8", "-isa=gfx1010"]),
    ("P2DCUL1", "spmv", ["-num-of-sa=8", "-isa=gfx1010"]),
    ("P2DCUL1", "atax", ["-num-of-sa=16", "-isa=gfx1010"]),
    ("P2DCUL1", "bicg", ["-num-of-sa=16", "-isa=gfx1010"]),
    ("P2DCUL1", "bitonicsort", ["-num-of-sa=16", "-isa=gfx1010"]),
    ("P2DCUL1", "fir", ["-num-of-sa=16", "-isa=gfx1010"]),
    ("P2DCUL1", "floydwarshall", ["-num-of-sa=16", "-isa=gfx1010"]),
    ("P2DCUL1", "fastwalshtransform", ["-num-of-sa=16", "-isa=gfx1010"]),
    ("P2DCUL1", "kmeans", ["-num-of-sa=16", "-isa=gfx1010"]),
    ("P2DCUL1", "matrixtranspose", ["-num-of-sa=16", "-isa=gfx1010"]),
    ("P2DCUL1", "pagerank", ["-num-of-sa=16", "-isa=gfx1010"]),
    ("P2DCUL1", "relu", ["-num-of-sa=16", "-isa=gfx1010"]),
    ("P2DCUL1", "spmv", ["-num-of-sa=16", "-isa=gfx1010"]),
    ("P2DCUL1", "atax", ["-num-of-sa=32", "-isa=gfx1010"]),
    ("P2DCUL1", "bicg", ["-num-of-sa=32", "-isa=gfx1010"]),
    ("P2DCUL1", "bitonicsort", ["-num-of-sa=32", "-isa=gfx1010"]),
    ("P2DCUL1", "fir", ["-num-of-sa=32", "-isa=gfx1010"]),
    ("P2DCUL1", "floydwarshall", ["-num-of-sa=32", "-isa=gfx1010"]),
    ("P2DCUL1", "fastwalshtransform", ["-num-of-sa=32", "-isa=gfx1010"]),
    ("P2DCUL1", "kmeans", ["-num-of-sa=32", "-isa=gfx1010"]),
    ("P2DCUL1", "matrixtranspose", ["-num-of-sa=32", "-isa=gfx1010"]),
    ("P2DCUL1", "pagerank", ["-num-of-sa=32", "-isa=gfx1010"]),
    ("P2DCUL1", "relu", ["-num-of-sa=32", "-isa=gfx1010"]),
    ("P2DCUL1", "spmv", ["-num-of-sa=32", "-isa=gfx1010"]),
    ("P2DCUL1", "atax", ["-num-of-sa=64", "-isa=gfx1010"]),
    ("P2DCUL1", "bicg", ["-num-of-sa=64", "-isa=gfx1010"]),
    ("P2DCUL1", "bitonicsort", ["-num-of-sa=64", "-isa=gfx1010"]),
    ("P2DCUL1", "fir", ["-num-of-sa=64", "-isa=gfx1010"]),
    ("P2DCUL1", "floydwarshall", ["-num-of-sa=64", "-isa=gfx1010"]),
    ("P2DCUL1", "fastwalshtransform", ["-num-of-sa=64", "-isa=gfx1010"]),
    ("P2DCUL1", "kmeans", ["-num-of-sa=64", "-isa=gfx1010"]),
    ("P2DCUL1", "matrixtranspose", ["-num-of-sa=64", "-isa=gfx1010"]),
    ("P2DCUL1", "pagerank", ["-num-of-sa=64", "-isa=gfx1010"]),
    ("P2DCUL1", "relu", ["-num-of-sa=64", "-isa=gfx1010"]),
    ("P2DCUL1", "spmv", ["-num-of-sa=64", "-isa=gfx1010"]),
]


def run_exp(exp):
    try:
        cwd = os.getcwd()
        file_name = f'{exp[0]}_{exp[1]}_{"_".join(exp[2])}'
        metic_file_name = file_name + "_metrics"
        cmd = (
            f"{cwd}/{exp[0]}/{exp[0]} -benchmark={exp[1]} -report-all "
            + f"-metric-file-name={metic_file_name} "
            + " ".join(exp[2])
        )
        print(cmd)

        out_file_name = file_name + "_out.stdout"

        out_file = open(out_file_name, "w")
        out_file.write(f"Executing {cmd}\n")
        start_time = datetime.now()
        out_file.write(f"Start time: {start_time}\n")
        out_file.flush()

        process = subprocess.Popen(
            cmd, shell=True, stdout=out_file, stderr=out_file, cwd=cwd
        )
        process.wait()

        end_time = datetime.now()
        out_file.write(f"End time: {end_time}\n")

        elapsed_time = end_time - start_time
        out_file.write(f"Elapsed time: {elapsed_time}\n")

        if process.returncode != 0:
            print("Error executing ", cmd)
        else:
            print("Executed ", cmd, ", time ", elapsed_time)

        out_file.close()
    except Exception as e:
        print(e)


def main():
    cwd = os.getcwd()

    process = subprocess.Popen("cd P2DCUL1 && go build", shell=True, cwd=cwd)
    process.wait()

    process = subprocess.Popen("cd P2DCU && go build", shell=True, cwd=cwd)
    process.wait()

    tp = ThreadPool(32)
    for exp in exps:
        tp.apply_async(run_exp, args=(exp,))

    tp.close()
    tp.join()


if __name__ == "__main__":
    main()
