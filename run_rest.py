import subprocess
import os
from multiprocessing.pool import ThreadPool
from datetime import datetime

exps = [
    # ("P2", "atax", ["-gpu=R9Nano", "-isa=gfx803"]),
    # ("P2", "bicg", ["-gpu=R9Nano", "-isa=gfx803"]),
    # ("P2", "bitonicsort", ["-gpu=R9Nano", "-isa=gfx803"]),
    # ("P2", "fir", ["-gpu=R9Nano", "-isa=gfx803"]),
    # ("P2", "floydwarshall", ["-gpu=R9Nano", "-isa=gfx803"]),
    # ("P2", "fastwalshtransform", ["-gpu=R9Nano", "-isa=gfx803"]),
    # ("P2", "kmeans", ["-gpu=R9Nano", "-isa=gfx803"]),
    # ("P2", "matrixtranspose", ["-gpu=R9Nano", "-isa=gfx803"]),
    # ("P2", "pagerank", ["-gpu=R9Nano", "-isa=gfx803"]),
    # ("P2", "relu", ["-gpu=R9Nano", "-isa=gfx803"]),
    # ("P2", "spmv", ["-gpu=R9Nano", "-isa=gfx803"]),
    # ("P2", "atax", ["-gpu=R9Nano", "-isa=gfx1010"]),
    # ("P2", "bicg", ["-gpu=R9Nano", "-isa=gfx1010"]),
    # ("P2", "bitonicsort", ["-gpu=R9Nano", "-isa=gfx1010"]),
    # ("P2", "fir", ["-gpu=R9Nano", "-isa=gfx1010"]),
    # ("P2", "floydwarshall", ["-gpu=R9Nano", "-isa=gfx1010"]),
    # ("P2", "fastwalshtransform", ["-gpu=R9Nano", "-isa=gfx1010"]),
    # ("P2", "kmeans", ["-gpu=R9Nano", "-isa=gfx1010"]),
    # ("P2", "matrixtranspose", ["-gpu=R9Nano", "-isa=gfx1010"]),
    # ("P2", "pagerank", ["-gpu=R9Nano", "-isa=gfx1010"]),
    # ("P2", "relu", ["-gpu=R9Nano", "-isa=gfx1010"]),
    # ("P2", "spmv", ["-gpu=R9Nano", "-isa=gfx1010"]),
    # ("P2DCU", "atax", ["-gpu=R9Nano", "-isa=gfx803"]),
    # ("P2DCU", "bicg", ["-gpu=R9Nano", "-isa=gfx803"]),
    # ("P2DCU", "bitonicsort", ["-gpu=R9Nano", "-isa=gfx803"]),
    # ("P2DCU", "fir", ["-gpu=R9Nano", "-isa=gfx803"]),
    # ("P2DCU", "floydwarshall", ["-gpu=R9Nano", "-isa=gfx803"]),
    # ("P2DCU", "fastwalshtransform", ["-gpu=R9Nano", "-isa=gfx803"]),
    # ("P2DCU", "kmeans", ["-gpu=R9Nano", "-isa=gfx803"]),
    # ("P2DCU", "matrixtranspose", ["-gpu=R9Nano", "-isa=gfx803"]),
    # ("P2DCU", "pagerank", ["-gpu=R9Nano", "-isa=gfx803"]),
    ("P2DCU", "relu", ["-gpu=R9Nano", "-isa=gfx803"]),
    ("P2DCU", "spmv", ["-gpu=R9Nano", "-isa=gfx803"]),
    # ("P2DCU", "atax", ["-gpu=R9Nano", "-isa=gfx1010"]),
    # ("P2DCU", "bicg", ["-gpu=R9Nano", "-isa=gfx1010"]),
    # ("P2DCU", "bitonicsort", ["-gpu=R9Nano", "-isa=gfx1010"]),
    # ("P2DCU", "fir", ["-gpu=R9Nano", "-isa=gfx1010"]),
    # ("P2DCU", "floydwarshall", ["-gpu=R9Nano", "-isa=gfx1010"]),
    # ("P2DCU", "fastwalshtransform", ["-gpu=R9Nano", "-isa=gfx1010"]),
    # ("P2DCU", "kmeans", ["-gpu=R9Nano", "-isa=gfx1010"]),
    # ("P2DCU", "matrixtranspose", ["-gpu=R9Nano", "-isa=gfx1010"]),
    # ("P2DCU", "pagerank", ["-gpu=R9Nano", "-isa=gfx1010"]),
    # ("P2DCU", "relu", ["-gpu=R9Nano", "-isa=gfx1010"]),
    # ("P2DCU", "spmv", ["-gpu=R9Nano", "-isa=gfx1010"]),
    # ("P4", "atax", ["-gpu=MI100", "-isa=gfx908"]),
    # ("P4", "bicg", ["-gpu=MI100", "-isa=gfx908"]),
    # ("P4", "bitonicsort", ["-gpu=MI100", "-isa=gfx908"]),
    # ("P4", "fastwalshtransform", ["-gpu=MI100", "-isa=gfx908"]),
    # ("P4", "fir", ["-gpu=MI100", "-isa=gfx908"]),
    # ("P4", "floydwarshall", ["-gpu=MI100", "-isa=gfx908"]),
    # ("P4", "kmeans", ["-gpu=MI100", "-isa=gfx908"]),
    # ("P4", "matrixtranspose", ["-gpu=MI100", "-isa=gfx908"]),
    # ("P4", "pagerank", ["-gpu=MI100", "-isa=gfx908"]),
    # ("P4", "relu", ["-gpu=MI100", "-isa=gfx908"]),
    # ("P4", "spmv", ["-gpu=MI100", "-isa=gfx908"]),
]


def run_exp(exp):
    try:
        cwd = os.getcwd()
        file_name = f'{exp[0]}_{exp[1]}_{"_".join(exp[2])}'
        metic_file_name = file_name + "_metrics"
        cmd = (
            f"{cwd}/{exp[0]}/{exp[0]} -benchmark={exp[1]} -report-all "
            + f"-metric-file-name={metic_file_name} "
            + " ".join(exp[2])
        )
        print(cmd)

        out_file_name = file_name + "_out.stdout"

        out_file = open(out_file_name, "w")
        out_file.write(f"Executing {cmd}\n")
        start_time = datetime.now()
        out_file.write(f"Start time: {start_time}\n")
        out_file.flush()

        process = subprocess.Popen(
            cmd, shell=True, stdout=out_file, stderr=out_file, cwd=cwd
        )
        process.wait()

        end_time = datetime.now()
        out_file.write(f"End time: {end_time}\n")

        elapsed_time = end_time - start_time
        out_file.write(f"Elapsed time: {elapsed_time}\n")

        if process.returncode != 0:
            print("Error executing ", cmd)
        else:
            print("Executed ", cmd, ", time ", elapsed_time)

        out_file.close()
    except Exception as e:
        print(e)


def main():
    cwd = os.getcwd()

    process = subprocess.Popen("cd P2DCU && go build", shell=True, cwd=cwd)
    process.wait()

    process = subprocess.Popen("cd P2 && go build", shell=True, cwd=cwd)
    process.wait()

    tp = ThreadPool(16)
    for exp in exps:
        tp.apply_async(run_exp, args=(exp,))

    tp.close()
    tp.join()


if __name__ == "__main__":
    main()
